import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_twitter_clone/helper/utility.dart';
import 'package:flutter_twitter_clone/model/push_notification_model.dart';
import 'package:rxdart/rxdart.dart';

/*
FCM Token :
dmuFSF1TJ0cJhC5a53iPNv:APA91bFKvTj7taHGcEelCYBF9PLUUE9UJJDl1cTlp9SarefyF42iYKuV6zFE7D864E18nMO72VwFJISaYBQCUXYuRULdsBvlbe9P4u_Ztl7WAwZCw4VlcZo3xEOcCrtHcc_VYxxKTxIE
*/

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  cprint("Handling a background message: ${message.messageId}");
}

class PushNotificationService {
  final FirebaseMessaging _firebaseMessaging;

  PushNotificationService(this._firebaseMessaging) {
    initializeMessages();
  }

  late StreamSubscription<RemoteMessage> _backgroundMessageSubscription;
  late PublishSubject<PushNotificationModel> _pushNotificationSubject;

  Stream<PushNotificationModel> get pushNotificationResponseStream =>
      _pushNotificationSubject.stream;

  void initializeMessages() async {
    configure();
  }

  void configure() async {
    _pushNotificationSubject = PublishSubject<PushNotificationModel>();

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      cprint('Got a message whilst in the foreground!');
      try {
        // var data = json.decode(message.data.toString()) as Map<String, dynamic>;
        myBackgroundMessageHandler(message.data, onMessage: true);
      } catch (e) {
        cprint(e, errorIn: "On Message");
      }
    });

    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

    /// Get message when the app is in the Terminated form
    FirebaseMessaging.instance.getInitialMessage().then((event) {
      if (event != null) {
        try {
          myBackgroundMessageHandler(event.data, onLaunch: true);
        } catch (e) {
          cprint(e, errorIn: "On getInitialMessage");
        }
      }
    });

    /// Returns a [Stream] that is called when a user presses a notification message displayed via FCM.
    _backgroundMessageSubscription =
        FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage? event) {
      if (event != null) {
        try {
          myBackgroundMessageHandler(event.data, onLaunch: true);
        } catch (e) {
          cprint(e, errorIn: "On onMessageOpenedApp");
        }
      }
    });
  }

  Future<String?> getDeviceToken() async {
    final token = await _firebaseMessaging.getToken(
        vapidKey:
            "BEGK-G5LdkZxkoNOhV3NEzmktUxh8Ka4YFd4khsKCw62uDdWLQ2y5JzsOXoRRgGix5MVQUGOEevDxGIDUrtIzLs");
    return token;
  }

  void myBackgroundMessageHandler(Map<String, dynamic> message,
      {bool onBackGround = false,
      bool onLaunch = false,
      bool onMessage = false,
      bool onResume = false}) async {
    try {
      if (!onMessage) {
        PushNotificationModel model = PushNotificationModel.fromJson(message);
        _pushNotificationSubject.add(model);
      }
    } catch (error) {
      cprint(error, errorIn: "myBackgroundMessageHandler");
    }
  }
}
