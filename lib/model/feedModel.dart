// ignore_for_file: avoid_print

import 'package:flutter_twitter_clone/model/user.dart';

class FeedModel {
  String? key;
  String? parentkey;
  String? childRetwetkey;
  String? description;
  late String userId;
  int? likeCount;
  List<String>? likeList;
  int? hateCount;
  List<String>? hateList;
  int? commentCount;
  int? retweetCount;
  late String createdAt;
  String? imagePath;
  List<String>? tags;
  List<String?>? replyTweetKeyList;
  String? lanCode; //Saving the language of the tweet so to not translate to check which language
  UserModel? user;
  // PJH, 추가
  List<String>? lstImagePath;
  String? feedTitle;
  String? feedContent;
  String? feedStore;

  FeedModel(
      {this.key,
      this.description,
      required this.userId,
      this.likeCount,
      this.commentCount,
      this.retweetCount,
      required this.createdAt,
      this.imagePath,
      this.likeList,
      this.tags,
      this.user,
      this.replyTweetKeyList,
      this.parentkey,
      this.lanCode,
      this.childRetwetkey,
      // PJH, 추가
      this.lstImagePath,
      this.feedTitle,
      this.feedContent,
      this.feedStore,
      this.hateCount,
      // YEJ, 추가
      this.hateList});

  toJson() {
    return {
      "userId": userId,
      "description": description,
      "likeCount": likeCount ?? 0,
      "commentCount": commentCount ?? 0,
      "retweetCount": retweetCount ?? 0,
      "createdAt": createdAt,
      "imagePath": imagePath,
      "likeList": likeList,
      "tags": tags,
      "replyTweetKeyList": replyTweetKeyList,
      "user": user == null ? null : user!.toJson(),
      "parentkey": parentkey,
      "lanCode": lanCode,
      "childRetwetkey": childRetwetkey,
      // PJH, 추가
      "lstImagePath": lstImagePath,
      "feedTitle": feedTitle,
      "feedContent": feedContent,
      "feedStore": feedStore,
      "hateCount": hateCount ?? 0,
      // YEJ, 추가
      "hateList": hateList
    };
  }

  FeedModel.fromJson(Map<dynamic, dynamic> map) {
    key = map['key'];
    description = map['description'];
    userId = map['userId'];
    //  name = map['name'];
    //  profilePic = map['profilePic'];
    likeCount = map['likeCount'] ?? 0;
    // YEJ, 추가
    hateCount = map['hateCount'] ?? 0;
    commentCount = map['commentCount'];
    retweetCount = map["retweetCount"] ?? 0;
    imagePath = map['imagePath'];
    createdAt = map['createdAt'];
    imagePath = map['imagePath'];
    lanCode = map['lanCode'];
    //  username = map['username'];
    user = UserModel.fromJson(map['user']);
    parentkey = map['parentkey'];
    childRetwetkey = map['childRetwetkey'];
    if (map['tags'] != null) {
      tags = <String>[];
      map['tags'].forEach((value) {
        tags!.add(value);
      });
    }
    if (map["likeList"] != null) {
      likeList = <String>[];
      final list = map['likeList'];
      if (list is List) {
        map['likeList'].forEach((value) {
          if (value is String) {
            likeList!.add(value);
          }
        });
        likeCount = likeList!.length;
      }
      else if (list is Map) {
        list.forEach((key, value) {
          likeList!.add(value["userId"]);
        });
        likeCount = list.length;
      }
    } else {
      likeList = [];
      likeCount = 0;
    }
    // YEJ, 추가
    if (map["hateList"] != null) {
      hateList = <String>[];
      final list = map['hateList'];
      if (list is List) {
        map['hateList'].forEach((value) {
          if (value is String) {
            hateList!.add(value);
          }
        });
        hateCount = hateList!.length;
      } else if (list is Map) {
        list.forEach((key, value) {
          hateList!.add(value["userId"]);
        });
        hateCount = list.length;
      }
    } else {
      hateList = [];
      hateCount = 0;
    }

    if (map['replyTweetKeyList'] != null) {
      map['replyTweetKeyList'].forEach((value) {
        replyTweetKeyList = <String>[];
        map['replyTweetKeyList'].forEach((value) {
          replyTweetKeyList!.add(value);
        });
      });
      commentCount = replyTweetKeyList!.length;
    } else {
      replyTweetKeyList = [];
      commentCount = 0;
    }
    // PJH, 추가
    if (map['lstImagePath'] != null) {
      lstImagePath = <String>[];
      map['lstImagePath'].forEach((value) {
        lstImagePath!.add(value);
      });
    } else {
      lstImagePath = [];
    }
    feedTitle = map['feedTitle'];
    feedContent = map['feedContent'];
    feedStore = map['feedStore'];
    hateCount = map['hateCount'] ?? 0;
  }

  bool get isValidTweet {
    bool isValid = false;
    if (user != null && user!.userName != null && user!.userName!.isNotEmpty) {
      isValid = true;
    } else {
      print("Invalid Tweet found. Id:- $key");
    }
    return isValid;
  }

  /// get tweet key to retweet.
  ///
  /// If tweet [TweetType] is [TweetType.Retweet] and its description is null
  /// then its retweeted child tweet will be shared.
  String get getTweetKeyToRetweet {
    if (description == null && imagePath == null && childRetwetkey != null) {
      return childRetwetkey!;
    } else {
      return key!;
    }
  }
}
