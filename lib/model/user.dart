import 'package:equatable/equatable.dart';

// ignore: must_be_immutable
class UserModel extends Equatable {
  String? key;
  String? email;
  String? userId;
  String? displayName;
  String? userName;
  String? webSite;
  String? profilePic;
  String? bannerImage;
  String? contact;
  String? bio;
  String? location;
  String? dob;
  String? createdAt;
  bool? isVerified;
  int? followers;
  int? following;
  String? fcmToken;
  List<String>? followersList;
  List<String>? followingList;
  // PJH, 계정정보 5개
  String? empNo;
  String? empNm;
  String? orgCd;
  String? dutyCd;
  String? dutyNm;
  String? orgNm;
  String? superOrgCd;
  String? superOrgNm;
  // YEJ, 푸쉬 여부
  bool? isPush;

  UserModel(
      {this.email,
      this.userId,
      this.displayName,
      this.profilePic,
      this.bannerImage,
      this.key,
      this.contact,
      this.bio,
      this.dob,
      this.location,
      this.createdAt,
      this.userName,
      this.followers,
      this.following,
      this.webSite,
      this.isVerified,
      this.fcmToken,
      this.followersList,
      this.followingList,
      // PJH, 계정정보 5개
      this.empNo,
      this.empNm,
      this.orgCd,
      this.dutyCd,
      this.dutyNm,
      this.orgNm,
      this.superOrgCd,
      this.superOrgNm,
      // YEJ, 푸쉬 여부
      this.isPush});

  UserModel.fromJson(Map<dynamic, dynamic>? map) {
    if (map == null) {
      return;
    }
    followersList ??= [];
    email = map['email'];
    userId = map['userId'];
    displayName = map['displayName'];
    profilePic = map['profilePic'];
    bannerImage = map['bannerImage'];
    key = map['key'];
    dob = map['dob'];
    bio = map['bio'];
    location = map['location'];
    contact = map['contact'];
    createdAt = map['createdAt'];
    followers = map['followers'];
    following = map['following'];
    userName = map['userName'];
    webSite = map['webSite'];
    fcmToken = map['fcmToken'];
    isVerified = map['isVerified'] ?? false;
    if (map['followerList'] != null) {
      followersList = <String>[];
      map['followerList'].forEach((value) {
        followersList!.add(value);
      });
    }
    followers = followersList != null ? followersList!.length : null;
    if (map['followingList'] != null) {
      followingList = <String>[];
      map['followingList'].forEach((value) {
        followingList!.add(value);
      });
    }
    following = followingList != null ? followingList!.length : null;
    // PJH, 계정정보 5개
    empNo = map['empNo'];
    empNm = map['empNm'];
    orgCd = map['orgCd'];
    dutyCd = map['dutyCd'];
    dutyNm = map['dutyNm'];
    orgNm = map['orgNm'];
    superOrgCd = map['superOrgCd'];
    superOrgNm = map['superOrgNm'];
    // YEJ, 푸쉬 여부
    isPush = map['isPush'];
  }
  toJson() {
    return {
      'key': key,
      'userId': userId,
      'email': email,
      'displayName': displayName,
      'profilePic': profilePic,
      'bannerImage': bannerImage,
      'contact': contact,
      'dob': dob,
      'bio': bio,
      'location': location,
      'createdAt': createdAt,
      'followers': followersList != null ? followersList!.length : null,
      'following': followingList != null ? followingList!.length : null,
      'userName': userName,
      'webSite': webSite,
      'isVerified': isVerified ?? false,
      'fcmToken': fcmToken,
      'followerList': followersList,
      'followingList': followingList,
      // PJH, 계정정보 5개
      'empNo': empNo,
      'empNm': empNm,
      'orgCd': orgCd,
      'dutyCd': dutyCd,
      'dutyNm': dutyNm,
      'orgNm': orgNm,
      'superOrgCd': superOrgCd,
      'superOrgNm': superOrgNm,
      // YEJ, 푸쉬 여부
      'isPush': isPush ?? true
    };
  }

  UserModel copyWith(
      {String? email,
      String? userId,
      String? displayName,
      String? profilePic,
      String? key,
      String? contact,
      String? bio,
      String? dob,
      String? bannerImage,
      String? location,
      String? createdAt,
      String? userName,
      int? followers,
      int? following,
      String? webSite,
      bool? isVerified,
      String? fcmToken,
      List<String>? followingList,
      List<String>? followersList,
      // PJH, 계정정보 5개
      String? empNo,
      String? empNm,
      String? orgCd,
      String? dutyCd,
      String? dutyNm,
      String? orgNm,
      String? superOrgCd,
      String? superOrgNm,
      // YEJ, 푸쉬 여부
      bool? isPush}) {
    return UserModel(
        email: email ?? this.email,
        bio: bio ?? this.bio,
        contact: contact ?? this.contact,
        createdAt: createdAt ?? this.createdAt,
        displayName: displayName ?? this.displayName,
        dob: dob ?? this.dob,
        followers: followers ?? this.followers,
        following: following ?? this.following,
        isVerified: isVerified ?? this.isVerified,
        key: key ?? this.key,
        location: location ?? this.location,
        profilePic: profilePic ?? this.profilePic,
        bannerImage: bannerImage ?? this.bannerImage,
        userId: userId ?? this.userId,
        userName: userName ?? this.userName,
        webSite: webSite ?? this.webSite,
        fcmToken: fcmToken ?? this.fcmToken,
        followersList: followersList ?? this.followersList,
        followingList: followingList ?? this.followingList,
        // PJH, 계정정보 5개
        empNo: empNo ?? this.empNo,
        empNm: empNm ?? this.empNm,
        orgCd: orgCd ?? this.orgCd,
        dutyCd: dutyCd ?? this.dutyCd,
        dutyNm: dutyNm ?? this.dutyNm,
        orgNm: orgNm ?? this.orgNm,
        superOrgCd: superOrgCd ?? this.superOrgCd,
        superOrgNm: superOrgNm ?? this.superOrgNm,
        //YEJ, 푸쉬 여부
        isPush: isPush ?? this.isPush);
  }

  String get getFollower {
    return '${followers ?? 0}';
  }

  String get getFollowing {
    return '${following ?? 0}';
  }

  @override
  List<Object?> get props => [
        key,
        email,
        userId,
        displayName,
        userName,
        webSite,
        profilePic,
        bannerImage,
        contact,
        bio,
        location,
        dob,
        createdAt,
        isVerified,
        followers,
        following,
        fcmToken,
        followersList,
        followingList,
        // PJH, 계정정보 5개
        empNo,
        empNm,
        orgCd,
        dutyCd,
        dutyNm,
        orgNm,
        superOrgCd,
        superOrgNm,
        // YEJ, 푸쉬 여부
        isPush
      ];
}
