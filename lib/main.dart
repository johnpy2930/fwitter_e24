import 'dart:convert';
import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_twitter_clone/helper/utility.dart';
import 'package:flutter_twitter_clone/state/suggestionUserState.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:flutter_twitter_clone/state/searchState.dart';
import 'package:flutter_twitter_clone/ui/page/common/locator.dart';
import 'package:flutter_twitter_clone/ui/theme/theme.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'helper/routes.dart';
import 'state/appState.dart';
import 'state/authState.dart';
import 'state/chats/chatState.dart';
import 'state/feedState.dart';
import 'state/notificationState.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  setupDependencies();

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    // YEJ, StatusBar 숨기기
    SystemChrome.setEnabledSystemUIOverlays([]);



    return MultiProvider(
      providers: [
        ChangeNotifierProvider<AppState>(create: (_) => AppState()),
        ChangeNotifierProvider<AuthState>(create: (_) => AuthState()),
        ChangeNotifierProvider<FeedState>(create: (_) => FeedState()),
        ChangeNotifierProvider<ChatState>(create: (_) => ChatState()),
        ChangeNotifierProvider<SearchState>(create: (_) => SearchState()),
        ChangeNotifierProvider<NotificationState>(create: (_) => NotificationState()),
        ChangeNotifierProvider<SuggestionsState>(create: (_) => SuggestionsState()),
      ],
      child: MaterialApp(
        title: 'Fwitter',
        /*
        theme: AppTheme.apptheme.copyWith(
          textTheme: GoogleFonts.mulishTextTheme(
            Theme.of(context).textTheme,
          ),
        ),
        */
        theme: ThemeData(
          backgroundColor: Colors.white,
          appBarTheme: AppBarTheme(
            backgroundColor: TwitterColor.white,
            systemOverlayStyle: SystemUiOverlayStyle.dark,
            elevation: 0,
          ),
          /*
            colorScheme: const ColorScheme(
                background: Colors.white,
                onPrimary: Colors.white,
                onBackground: Colors.black,
                onError: Colors.white,
                onSecondary: Colors.white,
                onSurface: Colors.black,
                error: Colors.red,
                primary: Color(0xffffa300),
                primaryVariant: Color(0xffffa300),
                secondary: AppColor.secondary,
                secondaryVariant: AppColor.darkGrey,
                surface: Colors.white,
                brightness: Brightness.light
            )
            */
        ),
        debugShowCheckedModeBanner: false,
        routes: Routes.route(),
        onGenerateRoute: (settings) => Routes.onGenerateRoute(settings),
        onUnknownRoute: (settings) => Routes.onUnknownRoute(settings),
        initialRoute: "SplashPage",
      ),
    );
  }
}
