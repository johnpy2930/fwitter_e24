import 'dart:async';
import 'dart:io';
import 'package:flutter/widgets.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/firebase_database.dart' as dabase;
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_twitter_clone/helper/enum.dart';
import 'package:flutter_twitter_clone/helper/shared_prefrence_helper.dart';
import 'package:flutter_twitter_clone/model/feedModel.dart';
import 'package:flutter_twitter_clone/helper/utility.dart';
import 'package:flutter_twitter_clone/model/user.dart';
import 'package:flutter_twitter_clone/state/appState.dart';
import 'package:flutter_twitter_clone/ui/page/common/locator.dart';
import 'package:link_preview_generator/link_preview_generator.dart' show WebInfo;
import 'package:path/path.dart' as path;
import 'package:translator/translator.dart';

// import 'package:flutter_chat_types/flutter_chat_types.dart' show PreviewData;
// import 'authState.dart';

class FeedState extends AppState {
  bool isBusy = false;

  dabase.Query? _feedQuery;

  Map<String, List<FeedModel>?>? tweetReplyMap = {};

  FeedModel? _tweetToReplyModel;
  FeedModel? get tweetToReplyModel => _tweetToReplyModel;

  late List<FeedModel> _commentlist;

  List<FeedModel>? _feedlist;
  List<FeedModel>? _tweetDetailModelList;
  List<FeedModel>? get tweetDetailModel => _tweetDetailModelList;
  List<FeedModel>? get feedlist {
    if (_feedlist == null) {
      return null;
    } else {
      return List.from(_feedlist!.reversed);
    }
  }

  List<FeedModel>? getTweetList(UserModel? userModel) {
    if (userModel == null) {
      return null;
    }

    List<FeedModel>? list;

    if (!isBusy && feedlist != null && feedlist!.isNotEmpty) {
      list = feedlist!.where((x) {
        // PJH, 주요 게시글 표시 (본인 게시글 포함)
        if (x.parentkey == null) {
          return true;
        } else {
          return false;
        }
      }).toList();
      if (list.isEmpty) {
        list = null;
      }
    }
    return list;
  }

  Map<String, WebInfo> _linkWebInfos = {};
  Map<String, WebInfo> get linkWebInfos => _linkWebInfos;
  Map<String, Translation?> _tweetsTranslations = {};
  Map<String, Translation?> get tweetsTranslations => _tweetsTranslations;

  set setTweetToReply(FeedModel model) {
    _tweetToReplyModel = model;
  }

  set setFeedModel(FeedModel model) {
    _tweetDetailModelList ??= [];

    /// [Skip if any duplicate tweet already present]

    _tweetDetailModelList!.add(model);
    cprint("Detail Tweet added. Total Tweet: ${_tweetDetailModelList!.length}");
    notifyListeners();
  }

  void addWebInfo(String url, WebInfo webInfo) {
    _linkWebInfos.addAll({url: webInfo});
  }

  void addTweetTranslation(String tweet, Translation? translation) {
    _tweetsTranslations.addAll({tweet: translation});
    notifyListeners();
  }

  void removeLastTweetDetail(String tweetKey) {
    if (_tweetDetailModelList != null && _tweetDetailModelList!.isNotEmpty) {
      // var index = _tweetDetailModelList.in
      FeedModel removeTweet = _tweetDetailModelList!.lastWhere((x) => x.key == tweetKey);
      _tweetDetailModelList!.remove(removeTweet);
      tweetReplyMap?.removeWhere((key, value) => key == tweetKey);
      cprint("Last index Tweet removed from list. Remaining Tweet: ${_tweetDetailModelList!.length}");
      notifyListeners();
    }
  }

  void clearAllDetailAndReplyTweetStack() {
    if (_tweetDetailModelList != null) {
      _tweetDetailModelList!.clear();
    }
    if (tweetReplyMap != null) {
      tweetReplyMap!.clear();
    }
    cprint('Empty tweets from stack');
  }

  void getDataFromDatabase() {
    try {
      isBusy = true;
      _feedlist = null;
      notifyListeners();
      kDatabase.child('tweet').once().then((DatabaseEvent event) {
        final snapshot = event.snapshot;
        _feedlist = <FeedModel>[];
        if (snapshot.value != null) {
          var map = snapshot.value as Map<dynamic, dynamic>?;
          if (map != null) {
            map.forEach((key, value) {
              var model = FeedModel.fromJson(value);
              model.key = key;
              if (model.isValidTweet) {
                _feedlist!.add(model);
              }
            });

            /// Sort Tweet by time
            /// It helps to display newest Tweet first.
            _feedlist!.sort((x, y) => DateTime.parse(x.createdAt).compareTo(DateTime.parse(y.createdAt)));
          }
        } else {
          _feedlist = null;
        }
        isBusy = false;
        notifyListeners();
      });
    } catch (error) {
      isBusy = false;
      cprint(error, errorIn: 'getDataFromDatabase');
    }
  }

  void getpostDetailFromDatabase(String? postID, {FeedModel? model}) async {
    try {
      FeedModel? _tweetDetail;
      if (model != null) {
        // set tweet data from tweet list data.
        // No need to fetch tweet from firebase db if data already present in tweet list
        _tweetDetail = model;
        setFeedModel = _tweetDetail;
        postID = model.key;
      } else {
        assert(postID != null);
        // Fetch tweet data from firebase
        kDatabase.child('tweet').child(postID!).once().then((DatabaseEvent event) {
          final snapshot = event.snapshot;
          if (snapshot.value != null) {
            var map = snapshot.value as Map<dynamic, dynamic>;
            _tweetDetail = FeedModel.fromJson(map);
            _tweetDetail!.key = snapshot.key!;
            setFeedModel = _tweetDetail!;
          }
        });
      }

      if (_tweetDetail != null) {
        // Fetch comment tweets
        _commentlist = <FeedModel>[];
        // Check if parent tweet has reply tweets or not
        if (_tweetDetail!.replyTweetKeyList != null && _tweetDetail!.replyTweetKeyList!.isNotEmpty) {
          for (String? x in _tweetDetail!.replyTweetKeyList!) {
            if (x == null) {
              return;
            }
            kDatabase.child('tweet').child(x).once().then((DatabaseEvent event) {
              final snapshot = event.snapshot;
              if (snapshot.value != null) {
                var commentmodel = FeedModel.fromJson(snapshot.value as Map);
                String key = snapshot.key!;
                commentmodel.key = key;

                /// add comment tweet to list if tweet is not present in [comment tweet ]list
                /// To reduce duplicacy
                if (!_commentlist.any((x) => x.key == key)) {
                  _commentlist.add(commentmodel);
                }
              } else {}
              if (x == _tweetDetail!.replyTweetKeyList!.last) {
                /// Sort comment by time
                /// It helps to display newest Tweet first.
                _commentlist.sort((x, y) => DateTime.parse(y.createdAt).compareTo(DateTime.parse(x.createdAt)));
                tweetReplyMap!.putIfAbsent(postID!, () => _commentlist);
                notifyListeners();
              }
            });
          }
        } else {
          tweetReplyMap!.putIfAbsent(postID!, () => _commentlist);
          notifyListeners();
        }
      }
    } catch (error) {
      cprint(error, errorIn: 'getpostDetailFromDatabase');
    }
  }

  void deleteTweet(String tweetId, TweetType type, {String? parentkey}) {
    try {
      /// Delete tweet if it is in nested tweet detail page
      kDatabase.child('tweet').child(tweetId).remove().then((_) {
        if (type == TweetType.Detail && _tweetDetailModelList != null && _tweetDetailModelList!.isNotEmpty) {
          _tweetDetailModelList!.remove(_tweetDetailModelList!);
          if (_tweetDetailModelList!.isEmpty) {
            _tweetDetailModelList = null;
          }
          cprint('Tweet deleted from nested tweet detail page tweet');
        }
      });
    } catch (error) {
      cprint(error, errorIn: 'deleteTweet');
    }
  }

  void addLikeToTweet(FeedModel tweet, String userId) {
    try {
      if (tweet.likeList != null && tweet.likeList!.isNotEmpty && tweet.likeList!.any((id) => id == userId)) {
        tweet.likeList!.removeWhere((id) => id == userId);
        tweet.likeCount = tweet.likeCount! - 1;
      } else {
        tweet.likeList ??= [];
        tweet.likeList!.add(userId);
        tweet.likeCount = tweet.likeCount! + 1;
      }
      kDatabase.child('tweet').child(tweet.key!).child('likeList').set(tweet.likeList);
      kDatabase.child('tweet').child(tweet.key!).child('likeCount').set(tweet.likeCount);
      kDatabase.child('notification').child(tweet.userId).child(tweet.key!).set({
        'type': tweet.likeList!.isEmpty ? null : NotificationType.Like.toString(),
        'updatedAt': tweet.likeList!.isEmpty ? null : DateTime.now().toUtc().toString(),
      });
    } catch (error) {
      cprint(error, errorIn: 'addLikeToTweet');
    }
  }

  void addHateToTweet(FeedModel tweet, String userId) {
    try {
      if (tweet.hateList != null && tweet.hateList!.isNotEmpty && tweet.hateList!.any((id) => id == userId)) {
        tweet.hateList!.removeWhere((id) => id == userId);
        tweet.hateCount = tweet.hateCount! - 1;
      } else {
        tweet.hateList ??= [];
        tweet.hateList!.add(userId);
        tweet.hateCount = tweet.hateCount! + 1;
      }
      kDatabase.child('tweet').child(tweet.key!).child('hateList').set(tweet.hateList);
      kDatabase.child('tweet').child(tweet.key!).child('hateCount').set(tweet.hateCount);
      kDatabase.child('notification').child(tweet.userId).child(tweet.key!).set({
        'type': tweet.hateList!.isEmpty ? null : NotificationType.Like.toString(),
        'updatedAt': tweet.hateList!.isEmpty ? null : DateTime.now().toUtc().toString(),
      });
    } catch (error) {
      cprint(error, errorIn: 'addHateToTweet');
    }
  }

  Future<FeedModel?> getTweetDetail(String tweetId) async {
    FeedModel _tweetDetail;
    final event = await kDatabase.child('tweet').child(tweetId).once();

    final snapshot = event.snapshot;
    if (snapshot.value != null) {
      var map = snapshot.value as Map<dynamic, dynamic>;
      _tweetDetail = FeedModel.fromJson(map);
      _tweetDetail.key = snapshot.key!;
      return _tweetDetail;
    } else {
      return null;
    }
  }

  Future<FeedModel?> deleteOnlyParent(String tweetId) async {
    FeedModel? feedModel = await getTweetDetail(tweetId);
    if (feedModel == null) {
      return null;
    }
    await kDatabase.child('tweet').child(tweetId).remove();
    return feedModel;
  }

  Future<void> deleteParent(String tweetId) async {
    print('[feedState.dart] deleteParent() Start');

    FeedModel? parentModel = await getTweetDetail(tweetId);
    if (parentModel == null) {
      return;
    }

    // Delete Sub Key
    await Future.forEach(parentModel.replyTweetKeyList!, (x) async {
      FeedModel? subModel = await getTweetDetail(x.toString());
      if (subModel != null && subModel.key != null) {
        print('[feedState.dart] deleteParent() Sub Dest : ${subModel.description}');
        await deleteParent(subModel.key!);
      }
    });

    // Delete Main Key
    if (parentModel != null && parentModel.key != null) {
      print('[feedState.dart] deleteParent() Parent Dest : ${parentModel.description}');
      await kDatabase.child('tweet').child(parentModel.key!).remove();
    }

    print('[feedState.dart] deleteParent() End');
  }

  Future<bool> databaseInit() {
    try {
      if (_feedQuery == null) {
        _feedQuery = kDatabase.child("tweet");
        _feedQuery!.onChildAdded.listen(_onTweetAdded);
        _feedQuery!.onChildChanged.listen(_onTweetChanged);
        _feedQuery!.onChildRemoved.listen(_onTweetRemoved);
      }

      return Future.value(true);
    } catch (error) {
      cprint(error, errorIn: 'databaseInit');
      return Future.value(false);
    }
  }

  Future<FeedModel?> fetchTweet(String postID) async {
    FeedModel? _tweetDetail;

    /// If tweet is availabe in feedlist then no need to fetch it from firebase
    if (feedlist!.any((x) => x.key == postID)) {
      _tweetDetail = feedlist!.firstWhere((x) => x.key == postID);
    }

    /// If tweet is not available in feedlist then need to fetch it from firebase
    else {
      cprint("Fetched from DB: " + postID);
      var model = await kDatabase.child('tweet').child(postID).once().then(
        (DatabaseEvent event) {
          final snapshot = event.snapshot;
          if (snapshot.value != null) {
            var map = snapshot.value as Map<dynamic, dynamic>;
            _tweetDetail = FeedModel.fromJson(map);
            _tweetDetail!.key = snapshot.key!;
            print(_tweetDetail!.description);
          }
        },
      );
      if (model != null) {
        _tweetDetail = model;
      } else {
        cprint("Fetched null value from  DB");
      }
    }
    return _tweetDetail;
  }

  Future<String?> createTweet(FeedModel model) async {
    ///  Create tweet in [Firebase kDatabase]
    isBusy = true;
    notifyListeners();
    String? tweetKey;
    try {
      DatabaseReference dbReference = kDatabase.child('tweet').push();

      await dbReference.set(model.toJson());

      tweetKey = dbReference.key;
    } catch (error) {
      cprint(error, errorIn: 'createTweet');
    }
    isBusy = false;
    notifyListeners();
    return tweetKey;
  }

  Future<String?> createReTweet(FeedModel model) async {
    String? tweetKey;
    try {
      tweetKey = await createTweet(model);
      if (_tweetToReplyModel != null) {
        if (_tweetToReplyModel!.retweetCount == null) {
          _tweetToReplyModel!.retweetCount = 0;
        }
        _tweetToReplyModel!.retweetCount = _tweetToReplyModel!.retweetCount! + 1;
        updateTweet(_tweetToReplyModel!);
      }
    } catch (error) {
      cprint(error, errorIn: 'createReTweet');
    }
    return tweetKey;
  }

  Future<String?> uploadFile(File file) async {
    try {
      print('[feedState.dart] uploadFile() Start');
      isBusy = true;
      notifyListeners();
      var storageReference = FirebaseStorage.instance.ref().child("tweetImage").child(path.basename(DateTime.now().toIso8601String() + file.path));
      await storageReference.putFile(file);

      var url = await storageReference.getDownloadURL();
      // ignore: unnecessary_null_comparison
      if (url != null) {
        print('[feedState.dart] uploadFile() End');
        return url;
      }
      print('[feedState.dart] uploadFile() End');
      return null;
    } catch (error) {
      cprint(error, errorIn: 'uploadFile');
      print('[feedState.dart] uploadFile() Error');
      return null;
    }
  }

  Future<String?> addcommentToPost(FeedModel replyTweet) async {
    try {
      isBusy = true;
      notifyListeners();
      // String tweetKey;
      if (_tweetToReplyModel != null) {
        FeedModel tweet = _feedlist!.firstWhere((x) => x.key == _tweetToReplyModel!.key);
        var json = replyTweet.toJson();
        DatabaseReference ref = kDatabase.child('tweet').push();
        await ref.set(json);
        tweet.replyTweetKeyList!.add(ref.key);
        await updateTweet(tweet);
        return ref.key;
      }
    } catch (error) {
      cprint(error, errorIn: 'addcommentToPost');
    }
    isBusy = false;
    notifyListeners();
  }

  Future<void> deleteFile(String url, String baseUrl) async {
    try {
      var filePath = url.split(".com/o/")[1];
      filePath = filePath.replaceAll(RegExp(r'%2F'), '/');
      filePath = filePath.replaceAll(RegExp(r'(\?alt).*'), '');
      //  filePath = filePath.replaceAll('tweetImage/', '');
      cprint('[Path]' + filePath);
      var storageReference = FirebaseStorage.instance.ref();
      await storageReference.child(filePath).delete().catchError((val) {
        cprint('[Error]' + val);
      }).then((_) {
        cprint('[Sucess] Image deleted');
      });
    } catch (error) {
      cprint(error, errorIn: 'deleteFile');
    }
  }

  Future<void> updateTweet(FeedModel model) async {
    await kDatabase.child('tweet').child(model.key!).set(model.toJson());
  }

  Future addBookmark(String tweetId) async {
    final pref = getIt<SharedPreferenceHelper>();
    var userId = await pref.getUserProfile().then((value) => value!.userId);
    DatabaseReference dbReference = kDatabase.child('bookmark').child(userId!).child(tweetId);
    await dbReference.set({"tweetId": tweetId, "created_at": DateTime.now().toUtc().toString()});
  }

  _onTweetChanged(DatabaseEvent event) {
    print('[feedState.dart] _onTweetChanged()');
    var model = FeedModel.fromJson(event.snapshot.value as Map<dynamic, dynamic>);
    model.key = event.snapshot.key!;
    if (_feedlist!.any((x) => x.key == model.key)) {
      var oldEntry = _feedlist!.lastWhere((entry) {
        return entry.key == event.snapshot.key;
      });
      _feedlist![_feedlist!.indexOf(oldEntry)] = model;
    }

    if (_tweetDetailModelList != null && _tweetDetailModelList!.isNotEmpty) {
      if (_tweetDetailModelList!.any((x) => x.key == model.key)) {
        var oldEntry = _tweetDetailModelList!.lastWhere((entry) {
          return entry.key == event.snapshot.key;
        });
        _tweetDetailModelList![_tweetDetailModelList!.indexOf(oldEntry)] = model;
      }
      if (tweetReplyMap != null && tweetReplyMap!.isNotEmpty) {
        if (true) {
          var list = tweetReplyMap![model.parentkey];
          if (list != null && list.isNotEmpty) {
            var index = list.indexOf(list.firstWhere((x) => x.key == model.key));
            list[index] = model;
          } else {
            list = [];
            list.add(model);
          }
        }
      }
    }
    // if (event.snapshot != null) {
    cprint('Tweet updated');
    isBusy = false;
    notifyListeners();
    // }
  }

  _onTweetAdded(DatabaseEvent event) {
    print('[feedState.dart] _onTweetAdded()');
    FeedModel tweet = FeedModel.fromJson(event.snapshot.value as Map);
    tweet.key = event.snapshot.key!;

    /// Check if Tweet is a comment
    _onCommentAdded(tweet);
    tweet.key = event.snapshot.key!;
    _feedlist ??= <FeedModel>[];
    if ((_feedlist!.isEmpty || _feedlist!.any((x) => x.key != tweet.key)) && tweet.isValidTweet) {
      _feedlist!.add(tweet);
      cprint('Tweet Added');
    }
    isBusy = false;
    notifyListeners();
  }

  _onCommentAdded(FeedModel tweet) {
    print('[feedState.dart] _onCommentAdded()');
    if (tweet.childRetwetkey != null) {
      /// if Tweet is a type of retweet then it can not be a comment.
      return;
    }
    print('[feedState.dart] _onCommentAdded() tweetReplyMap ${tweet.description}');
    print('[feedState.dart] _onCommentAdded() tweetReplyMap is null');
    // tweetReplyMap ? 상위 트위터의 하위 트위터 리스트
    if (tweetReplyMap != null && tweetReplyMap!.isNotEmpty) {
      print('[feedState.dart] _onCommentAdded() tweetReplyMap is not null');
      // tweetReplyMap[상위 트위터] ? 하위 트위터 리스트
      if (tweetReplyMap![tweet.parentkey] != null) {
        // tweetReplyMap[상위 트위터] 하위 트위터 모델 추가
        tweetReplyMap![tweet.parentkey]!.insert(0, tweet);
      } else {
        tweetReplyMap![tweet.parentkey!] = [tweet];
      }
      cprint('Comment Added');
    }
    isBusy = false;
    notifyListeners();
  }

  _onTweetRemoved(DatabaseEvent event) async {
    print('[feedState.dart] _onTweetRemoved()');
    FeedModel tweet = FeedModel.fromJson(event.snapshot.value as Map);
    tweet.key = event.snapshot.key!;
    var tweetId = tweet.key;
    var parentkey = tweet.parentkey;

    ///  Delete tweet in [Home Page]
    try {
      late FeedModel deletedTweet;
      if (_feedlist!.any((x) => x.key == tweetId)) {
        /// Delete tweet if it is in home page tweet.
        deletedTweet = _feedlist!.firstWhere((x) => x.key == tweetId);
        _feedlist!.remove(deletedTweet);

        if (deletedTweet.parentkey != null && _feedlist!.isNotEmpty && _feedlist!.any((x) => x.key == deletedTweet.parentkey)) {
          // Decrease parent Tweet comment count and update
          var parentModel = _feedlist!.firstWhere((x) => x.key == deletedTweet.parentkey);
          parentModel.replyTweetKeyList!.remove(deletedTweet.key);
          parentModel.commentCount = parentModel.replyTweetKeyList!.length;
          updateTweet(parentModel);
        }
        if (_feedlist!.isEmpty) {
          _feedlist = null;
        }
        cprint('Tweet deleted from home page tweet list');
      }

      /// [Delete tweet] if it is in nested tweet detail comment section page
      if (parentkey != null && parentkey.isNotEmpty && tweetReplyMap != null && tweetReplyMap!.isNotEmpty && tweetReplyMap!.keys.any((x) => x == parentkey)) {
        // (type == TweetType.Reply || tweetReplyMap.length > 1) &&
        deletedTweet = tweetReplyMap![parentkey]!.firstWhere((x) => x.key == tweetId);
        tweetReplyMap![parentkey]!.remove(deletedTweet);
        if (tweetReplyMap![parentkey]!.isEmpty) {
          tweetReplyMap![parentkey] = null;
        }

        if (_tweetDetailModelList != null && _tweetDetailModelList!.isNotEmpty && _tweetDetailModelList!.any((x) => x.key == parentkey)) {
          var parentModel = _tweetDetailModelList!.firstWhere((x) => x.key == parentkey);
          parentModel.replyTweetKeyList!.remove(deletedTweet.key);
          parentModel.commentCount = parentModel.replyTweetKeyList!.length;
          cprint('Parent tweet comment count updated on child tweet removal');
          updateTweet(parentModel);
        }

        cprint('Tweet deleted from nested tweet detail comment section');
      }

      /// Delete tweet image from firebase storage if exist.
      if (deletedTweet.imagePath != null && deletedTweet.imagePath!.isNotEmpty) {
        deleteFile(deletedTweet.imagePath!, 'tweetImage');
      }

      /// If a retweet is deleted then retweetCount of original tweet should be decrease by 1.
      if (deletedTweet.childRetwetkey != null) {
        await fetchTweet(deletedTweet.childRetwetkey!).then((FeedModel? retweetModel) {
          if (retweetModel == null) {
            return;
          }
          if (retweetModel.retweetCount! > 0) {
            retweetModel.retweetCount = retweetModel.retweetCount! - 1;
          }
          updateTweet(retweetModel);
        });
      }

      /// Delete notification related to deleted Tweet.
      if (deletedTweet.likeCount! > 0) {
        kDatabase.child('notification').child(tweet.userId).child(tweet.key!).remove();
      }
      notifyListeners();
    } catch (error) {
      cprint(error, errorIn: '_onTweetRemoved');
    }
  }
}
