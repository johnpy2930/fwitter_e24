import 'dart:async';
import 'dart:math';
import 'package:flutter/services.dart';
import 'package:flutter_twitter_clone/helper/constant.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_twitter_clone/helper/shared_prefrence_helper.dart';
import 'package:flutter_twitter_clone/helper/utility.dart';
import 'package:flutter_twitter_clone/model/bookmarkModel.dart';
import 'package:flutter_twitter_clone/model/feedModel.dart';
import 'package:flutter_twitter_clone/model/user.dart';
import 'package:flutter_twitter_clone/state/authState.dart';
import 'package:flutter_twitter_clone/ui/page/common/locator.dart';
import 'package:provider/provider.dart';
import 'package:csv/csv.dart';
import 'appState.dart';

class ManagementState extends AppState {
  // 변수
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  // 생성자
  ManagementState() {}

  // 점포 저장
  void saveStore() {}

  // 점포 로드
  void loadStore() async {
    var snapshot = await kDatabase.child('e24_store').once();
  }

  // 점포 업로드
  void importStore(context) async {
    // Load Excel
    var data = await rootBundle.loadString('assets/files/store.csv');
    var lstExcel = CsvToListConverter().convert(data);
    var n_success = 1;
    var n_failure = 0;
    var total = lstExcel.length - n_success;
    for (int i = 1; i < lstExcel.length; i++) {
      var lstItem = lstExcel[i];
      var str_cd = lstItem[0].toString();
      var str_nm = lstItem[1].toString();
      var bizco_reg_bizcnd_nm = lstItem[13].toString();
      var bizco_reg_bizfld_nm = lstItem[14].toString();
      var repr_nm = lstItem[15].toString();
      var addr = lstItem[19].toString();
      var lgtd_val = lstItem[69].toString();
      var lttd_val = lstItem[70].toString();

      try {
        DatabaseReference dbReference =
            kDatabase.child('e24_store').child(str_cd);
        await dbReference.set({
          "str_cd": str_cd,
          "str_nm": str_nm,
          "bizco_reg_bizcnd_nm": bizco_reg_bizcnd_nm,
          "bizco_reg_bizfld_nm": bizco_reg_bizfld_nm,
          "repr_nm": repr_nm,
          "addr": addr,
          "lgtd_val": lgtd_val,
          "lttd_val": lttd_val
        });
        n_success += 1;
      } catch (error) {
        n_failure += 1;
      }
      print(
          'Success : ${n_success}/${total - n_failure}, Failure : ${n_failure}');
    }
  }

  // 조직도 업로드
  void importORG(context) async {
    try {
      // Load Excel
      var data = await rootBundle.loadString('assets/files/org.csv');
      var lstExcel = CsvToListConverter().convert(data);
      print('업로드 시작');
      for (int i = 1; i < lstExcel.length; i++) {
        var lstItem = lstExcel[i];
        var org_cd = lstItem[0].toString();
        var org_nm = lstItem[1].toString();
        var super_org_cd = lstItem[2].toString();
        var sta_ymd = lstItem[3].toString();

        // Save DB
        DatabaseReference dbReference =
            kDatabase.child('e24_org').child(org_cd);
        await dbReference.set({"org_nm": org_nm, "super_org_cd": super_org_cd});
      }
      print('업로드 끝');
    } catch (error) {
      cprint(error, errorIn: 'importORG');
    }
  }

  // 계정 업로드
  void importUser(context) async {
    var data = await rootBundle.loadString('assets/files/user_system.csv');
    var lstExcel = CsvToListConverter().convert(data);
    var n_success = 1;
    var n_failure = 0;
    var total = lstExcel.length - n_success;
    print('계정 업로드 시작');
    for (int i = 1; i < lstExcel.length; i++) {
      var lstItem = lstExcel[i];
      var empNo = lstItem[0].toString().padLeft(6, '0'); // EMP_NO
      var empNm = lstItem[1].toString(); // EMP_NM
      var orgCd = lstItem[2].toString(); // ORG_CD
      var dutyCd = lstItem[3].toString(); // DUTY_CD
      var dutyNm = lstItem[4].toString(); // DUTY_NM

      // Sign Up
      try {
        signUp(context, empNo, empNm, orgCd, dutyCd, dutyNm);
      } catch (error) {}
      print(
          'Success : ${n_success}/${total - n_failure}, Failure : ${n_failure}');
    }
    print('계정 업로드 끝');
  }

  // 조직도 조회
  Future<Map?> searchOrg(orgCd) async {
    var snapshot = await kDatabase.child('e24_org').child(orgCd).get();
    if (snapshot.exists && snapshot.value != null) {
      var data = snapshot.value as Map;
      return data;
    } else {
      print('No Exist');
    }
    return null;
  }

  // 계정 생성
  Future signUp(context, empNo, empNm, orgCd, dutyCd, dutyNm) async {
    try {
      Random random = Random();
      int randomNumber = random.nextInt(8);

      var profilePic = Constants.dummyProfilePicList[randomNumber];
      var email = '$empNo@shinsegae.com';
      var orgNm = '';
      var superOrgNm = '';
      var superOrgCd = '';

      if (orgCd != '') {
        var mapOrg = await searchOrg(orgCd);
        if (mapOrg != null) {
          orgNm = mapOrg['org_nm'].toString();
          superOrgCd = mapOrg['super_org_cd'].toString();
        }
      }

      if (superOrgCd != '') {
        var mapSuperOrg = await searchOrg(orgCd);
        if (mapSuperOrg != null) {
          superOrgNm = mapSuperOrg['org_nm'].toString();
        }
      }

      // UserModel 생성
      UserModel userModel = UserModel(
          email: email.toLowerCase(),
          bio: 'Edit profile to update bio',
          // contact:  _mobileController.text,
          displayName: empNm,
          dob: DateTime(1950, DateTime.now().month, DateTime.now().day + 3)
              .toString(),
          location: 'Somewhere in universe',
          profilePic: profilePic,
          isVerified: false,
          // PJH, 계정정보 5개
          empNo: empNo,
          empNm: empNm,
          orgCd: orgCd,
          dutyCd: dutyCd,
          dutyNm: dutyNm,
          orgNm: orgNm,
          superOrgNm: superOrgNm,
          superOrgCd: superOrgCd);

      var result = await _firebaseAuth.createUserWithEmailAndPassword(
        email: userModel.email!,
        password: empNo,
      );

      result.user!.updateDisplayName(userModel.displayName);
      result.user!.updatePhotoURL(userModel.profilePic);

      userModel.key = result.user!.uid;
      userModel.userId = result.user!.uid;

      var authState = Provider.of<AuthState>(context, listen: false);
      authState.createUser(userModel, newUser: true);
    } catch (error) {
      cprint(error, errorIn: 'signUp');
    }
  }

  // 데이터 생성
  Future saveData() async {
    try {
      DatabaseReference dbReference = kDatabase.child('store');
      await dbReference.set({"id": "12345", "name": "abc"});
    } catch (error) {
      cprint(error, errorIn: 'saveData');
    }
  }

  // 데이터 조회
  void searchData() async {
    var snapshot = await kDatabase.child('e24_org').child('CV0323323').get();
    if (snapshot.exists && snapshot.value != null) {
      var data = snapshot.value as Map;
    } else {
      print('No Exist');
    }
  }

  // 데이터 로드 (Once)
  void loadData() async {
    try {
      kDatabase.child('store').once().then((DatabaseEvent event) async {
        var snapshot = event.snapshot;
        print('loadData()');
      });
    } catch (error) {
      cprint(error, errorIn: 'loadData');
    }
  }
}
