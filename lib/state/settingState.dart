import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_twitter_clone/state/appState.dart';

class SettingState extends AppState {
  // 알림 상태 변경
  void updatePush(String profileId, bool state) async {
    DatabaseReference ref =
        FirebaseDatabase.instance.ref("profile/${profileId}");
    DatabaseEvent event = await ref.once();

    if (event.snapshot.exists && event.snapshot.value != null) {
      await ref.update({'isPush': state});
    } else {}
  }
}
