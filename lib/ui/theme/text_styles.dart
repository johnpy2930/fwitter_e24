part of 'theme.dart';

class TextStyles {
  TextStyles._();

  static TextStyle get onPrimaryTitleText {
    return const TextStyle(color: Colors.white, fontWeight: FontWeight.w600);
  }

  static TextStyle get onPrimarySubTitleText {
    return const TextStyle(
      color: Colors.white,
    );
  }

  static TextStyle get titleStyle {
    return const TextStyle(
      color: Colors.black,
      fontSize: 16,
      fontWeight: FontWeight.bold,
    );
  }

  static TextStyle get subtitleStyle {
    return const TextStyle(
        color: AppColor.darkGrey, fontSize: 14, fontWeight: FontWeight.bold);
  }

  static TextStyle get userNameStyle {
    return const TextStyle(
        color: AppColor.darkGrey, fontSize: 14, fontWeight: FontWeight.bold);
  }

  static TextStyle get textStyle14 {
    return const TextStyle(
        color: AppColor.darkGrey, fontSize: 14, fontWeight: FontWeight.bold);
  }

  /* 이마트24 스타일 추가 */

  // PJH, 피드 - 제목
  static TextStyle get eFeedTitleStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 16, fontWeight: FontWeight.normal);
  }

  // PJH, 피드 - 이름
  static TextStyle get eFeedNameStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold);
  }

  // PJH, 피드 - 부서
  static TextStyle get eFeedOrgStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 10, fontWeight: FontWeight.normal);
  }

  // PJH, 피드 - 날짜
  static TextStyle get eFeedDateStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 9, fontWeight: FontWeight.normal);
  }

  // PJH, 피드 - 점포
  static TextStyle get eFeedStoreStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 12, fontWeight: FontWeight.bold);
  }

  // PJH, 피드 - 카운트
  static TextStyle get eFeedCountStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 12, fontWeight: FontWeight.normal);
  }

  // PJH, 상세 피드 - 이름
  static TextStyle get eDetailFeedNameStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 10, fontWeight: FontWeight.bold);
  }

  // PJH, 상세 피드 - 부서
  static TextStyle get eDetailFeedOrgStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 10, fontWeight: FontWeight.normal);
  }

  // PJH, 상세 피드 - 내용
  static TextStyle get eDetailFeedContentStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 12, fontWeight: FontWeight.normal);
  }

  // PJH, 상세 피드 - 제목 1
  static TextStyle get eFeedDetailTitle1Style {
    return const TextStyle(
        color: Colors.black, fontSize: 10, fontWeight: FontWeight.bold);
  }

  // PJH, 상세 피드 - 제목 2
  static TextStyle get eFeedDetailTitle2Style {
    return const TextStyle(
        color: Colors.black, fontSize: 14, fontWeight: FontWeight.normal);
  }

  // PJH, 상세 피드 - 버튼 1 (Normal)
  static TextStyle get eDetailFeedButton1NormalStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 10, fontWeight: FontWeight.normal);
  }

  // PJH, 상세 피드 - 버튼 1 (Bold)
  static TextStyle get eDetailFeedButton1BoldStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 10, fontWeight: FontWeight.bold);
  }

  // YEJ, 상세 피드 - 날짜
  static TextStyle get eDetailFeedDateStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 10, fontWeight: FontWeight.bold);
  }

  // PJH, 작성 - 점포 Hint
  static TextStyle get eEweetStoreHintStyle {
    return const TextStyle(
        color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold);
  }

  // PJH, 작성 - 내용 Hint
  static TextStyle get eEweetContentHintStyle {
    return const TextStyle(
        color: Colors.grey, fontSize: 14, fontWeight: FontWeight.bold);
  }

  // PJH, 작성 - 제목 1 (Normal)
  static TextStyle get eEweetTitle1NormalStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 10, fontWeight: FontWeight.normal);
  }

  // PJH, 작성 - 제목 1 (Bold)
  static TextStyle get eEweetTitle1BoldStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 10, fontWeight: FontWeight.bold);
  }

  // PJH, 작성 - 제목 2 (Normal)
  static TextStyle get eEweetTitle2NormalStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 14, fontWeight: FontWeight.normal);
  }

  // PJH, 작성 - 제목 2 (Bold)
  static TextStyle get eEweetTitle2BoldStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 14, fontWeight: FontWeight.bold);
  }

  // PJH, 피드 - 점포 목록
  static TextStyle get eEweetStoreListStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 14, fontWeight: FontWeight.normal);
  }

  // YEJ, 프로필 - 이름
  static TextStyle get eProfileNameStyle {
    return const TextStyle(
        color: Colors.black, fontSize: 16, fontWeight: FontWeight.bold);
  }
  // YEJ, 프로필 - 이름
  static TextStyle get eProfileOrgStyle {
    return const TextStyle(
        color: Colors.grey, fontSize: 14, fontWeight: FontWeight.normal);
  }

}
