import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_twitter_clone/helper/utility.dart';
import 'package:flutter_twitter_clone/main.dart';
import 'package:flutter_twitter_clone/ui/page/profile/widgets/circular_image.dart';
import 'package:flutter_twitter_clone/helper/customRoute.dart';
import 'package:flutter_twitter_clone/helper/enum.dart';
import 'package:flutter_twitter_clone/model/feedModel.dart';
import 'package:flutter_twitter_clone/state/authState.dart';
import 'package:flutter_twitter_clone/state/feedState.dart';
import 'package:flutter_twitter_clone/widgets/cache_image.dart';
import 'package:flutter_twitter_clone/ui/theme/theme.dart';
import 'package:flutter_twitter_clone/widgets/customWidgets.dart';
import 'package:flutter_twitter_clone/widgets/tweet/tweet.dart';
import 'package:flutter_twitter_clone/widgets/tweet/widgets/tweetBottomSheet.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:photo_view/photo_view.dart';

class FeedPostDetail extends StatefulWidget {
  const FeedPostDetail({Key? key, required this.postId}) : super(key: key);
  final String postId;

  static Route<void> getRoute(String postId) {
    return SlideLeftRoute<void>(
      builder: (BuildContext context) => FeedPostDetail(
        postId: postId,
      ),
    );
  }

  @override
  _FeedPostDetailState createState() => _FeedPostDetailState();
}

class _FeedPostDetailState extends State<FeedPostDetail> {
  // PJH, 테두리
  final borderWidth = 0.1;
  //final borderColor = Colors.red;
  final borderColor = Colors.transparent;

  late String postId;

  final commentSize = 20.0;
  final commentCountSize = 25.0;
  final commentSpaceWidth = 5.0;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  var toggleMore = false;

  @override
  void initState() {
    postId = widget.postId;
    super.initState();
  }

  Future<bool> addComment(bool bSecond, String pstId, List<Widget> lstComment) async {
    print('[feedPostDetail.dart] addComment() Start');

    FeedModel? feedModel = await getTweetDetail(pstId);
    if (feedModel == null) {
      return false;
    }

    String displayParentName = '';
    if (feedModel.parentkey != null) {
      FeedModel? feedParentModel = await getTweetDetail(feedModel.parentkey!);
      if (feedParentModel != null && feedParentModel.user != null) {
        if (feedParentModel.user!.displayName != null) {
          displayParentName = feedParentModel.user!.displayName!;
        }
      }
    }
    print('Parent Display Name : ${displayParentName}');

    lstComment.add(bSecond
        ?
        // 대댓글
        Container(
            decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
            child: Column(
              children: [
                // PJH, 공백
                SizedBox(
                  height: 12,
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: borderWidth,
                        color: borderColor,
                      ),
                    ),
                  ),
                ),
                // PJH, 프로필, 이름, 조직, 날짜
                Container(
                  decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      // PJH, 공백
                      SizedBox(
                        width: 20,
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: borderWidth,
                              color: borderColor,
                            ),
                          ),
                        ),
                      ),
                      // PJH, 프로필, 이름, 조직 (왼쪽)
                      Container(
                        decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            // PJH, 공백
                            SizedBox(
                              width: 15,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: borderWidth,
                                    color: borderColor,
                                  ),
                                ),
                              ),
                            ),
                            // PJH, 프로필
                            SizedBox(
                              width: 32,
                              height: 32,
                              child: Container(
                                child: CircularImage(
                                  path: feedModel.user!.profilePic,
                                  empNo: feedModel.user!.empNo,
                                  isBorder: true,
                                  borderWidth: 0.5,
                                  borderColor: Colors.grey,
                                ),
                              ),
                            ),
                            // PJH, 공백
                            SizedBox(
                              width: 5,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: borderWidth,
                                    color: borderColor,
                                  ),
                                ),
                              ),
                            ),
                            // PJH, 이름
                            SizedBox(
                              child: Container(
                                decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                child: customText(
                                  '${feedModel.user!.displayName}',
                                  style: TextStyles.eDetailFeedNameStyle,
                                ),
                              ),
                            ),
                            // PJH, 공백
                            SizedBox(
                              width: 5,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: borderWidth,
                                    color: borderColor,
                                  ),
                                ),
                              ),
                            ),
                            // PJH, 조직도
                            SizedBox(
                              child: Container(
                                decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                child: customText(
                                  '${feedModel.user!.orgNm}',
                                  style: TextStyles.eDetailFeedOrgStyle,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                // PJH, 내용 & 사진
                Container(
                  decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                  child: Row(
                    children: [
                      // PJH, 공백
                      SizedBox(
                        width: 35,
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: borderWidth,
                              color: borderColor,
                            ),
                          ),
                        ),
                      ),
                      // PJH, 내용
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(width: borderWidth, color: borderColor),
                          ),
                          padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
                          child: customText(
                            '${feedModel.description ?? ""}',
                            style: TextStyles.eDetailFeedContentStyle,
                          ),
                        ),
                      ),
                      // PJH, 공백
                      SizedBox(
                        width: 15,
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: borderWidth,
                              color: borderColor,
                            ),
                          ),
                        ),
                      ),
                      // PJH, 사진
                      feedModel.lstImagePath!.isNotEmpty
                          ? Container(
                              width: 55,
                              height: 55,
                              child: InkWell(
                                onTap: () {
                                  showDialog(
                                    useSafeArea: false,
                                    context: context,
                                    builder: (_) => Dialog(
                                      elevation: 0,
                                      backgroundColor: Colors.black,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(width: borderWidth, color: borderColor),
                                        ),
                                        child: CarouselSlider(
                                          options: CarouselOptions(
                                            height: 320.0,
                                            viewportFraction: 0.6,
                                            enlargeCenterPage: true,
                                          ),
                                          items: feedModel.lstImagePath!.map((filePath) {
                                            return Builder(
                                              builder: (BuildContext context) {
                                                return Container(
                                                  decoration: BoxDecoration(
                                                    border: Border.all(width: borderWidth, color: borderColor),
                                                  ),
                                                  margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      showDialog(
                                                        context: context,
                                                        builder: (_) => showPicture(context, filePath),
                                                      );
                                                    },
                                                    child: CacheImage(
                                                      path: filePath,
                                                      fit: BoxFit.cover,
                                                      borderRadius: 6.0,
                                                    ),
                                                  ),
                                                );
                                              },
                                            );
                                          }).toList(),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                                child: CacheImage(
                                  path: feedModel.lstImagePath![0],
                                  fit: BoxFit.cover,
                                  borderRadius: 5.0,
                                ),
                              ),
                            )
                          : Container(),
                      // PJH, 공백
                      feedModel.lstImagePath!.isNotEmpty
                          ? SizedBox(
                              width: 15,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: borderWidth,
                                    color: borderColor,
                                  ),
                                ),
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ),
                // PJH, 댓글
                Container(
                  decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      // PJH, 공백
                      SizedBox(
                        width: 40,
                      ),
                      // PJH, 날짜
                      Container(
                        alignment: Alignment.centerRight,
                        decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                        child: customText('${feedModel.createdAt.substring(0, 19)}', style: TextStyles.eDetailFeedButton1NormalStyle),
                      ),
                      // PJH, 공백
                      SizedBox(
                        width: 10,
                      ),
                      // PJH, 댓글쓰기
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: borderWidth,
                            color: borderColor,
                          ),
                        ),
                        child: InkWell(
                          onTap: () {
                            var state = Provider.of<FeedState>(context, listen: false);
                            state.setTweetToReply = feedModel;
                            Navigator.of(context).pushNamed('/ComposeTweetPage/');
                          },
                          child: customText(
                            '댓글 쓰기',
                            style: TextStyles.eDetailFeedButton1BoldStyle,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                // PJH, 공백
                SizedBox(
                  height: 12,
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: borderWidth,
                        color: borderColor,
                      ),
                    ),
                  ),
                ),
                // PJH, 라인
                Container(
                  height: 0.6,
                  color: Colors.grey,
                )
              ],
            ),
          )
        :
        // 댓글
        Container(
            decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
            child: Column(
              children: [
                // PJH, 공백
                SizedBox(
                  height: 12,
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: borderWidth,
                        color: borderColor,
                      ),
                    ),
                  ),
                ),
                // PJH, 프로필, 이름, 조직, 날짜
                Container(
                  decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      // PJH, 프로필, 이름, 조직 (왼쪽)
                      Container(
                        decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                        child: Row(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            // PJH, 공백
                            SizedBox(
                              width: 15,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: borderWidth,
                                    color: borderColor,
                                  ),
                                ),
                              ),
                            ),
                            // PJH, 프로필
                            SizedBox(
                              width: 32,
                              height: 32,
                              child: Container(
                                child: CircularImage(
                                  path: feedModel.user!.profilePic,
                                  empNo: feedModel.user!.empNo,
                                  isBorder: true,
                                  borderWidth: 0.5,
                                  borderColor: Colors.grey,
                                ),
                              ),
                            ),
                            // PJH, 공백
                            SizedBox(
                              width: 5,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: borderWidth,
                                    color: borderColor,
                                  ),
                                ),
                              ),
                            ),
                            // PJH, 이름
                            SizedBox(
                              child: Container(
                                decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                child: customText(
                                  '${feedModel.user!.displayName}',
                                  style: TextStyles.eDetailFeedNameStyle,
                                ),
                              ),
                            ),
                            // PJH, 공백
                            SizedBox(
                              width: 5,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: borderWidth,
                                    color: borderColor,
                                  ),
                                ),
                              ),
                            ),
                            // PJH, 조직도
                            SizedBox(
                              child: Container(
                                decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                child: customText(
                                  '${feedModel.user!.orgNm}',
                                  style: TextStyles.eDetailFeedOrgStyle,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                // PJH, 내용 & 사진
                Container(
                  decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                  child: Row(
                    children: [
                      // PJH, 공백
                      SizedBox(
                        width: 20,
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: borderWidth,
                              color: borderColor,
                            ),
                          ),
                        ),
                      ),
                      // PJH, 내용
                      Expanded(
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(width: borderWidth, color: borderColor),
                          ),
                          padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
                          child: customText(
                            '${feedModel.description ?? ""}',
                            style: TextStyles.eDetailFeedContentStyle,
                          ),
                        ),
                      ),
                      // PJH, 공백
                      SizedBox(
                        width: 15,
                        child: Container(
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: borderWidth,
                              color: borderColor,
                            ),
                          ),
                        ),
                      ),
                      // PJH, 사진
                      feedModel.lstImagePath!.isNotEmpty
                          ? Container(
                              width: 55,
                              height: 55,
                              child: InkWell(
                                onTap: () {
                                  showDialog(
                                    useSafeArea: false,
                                    context: context,
                                    builder: (_) => Dialog(
                                      elevation: 0,
                                      backgroundColor: Colors.black,
                                      child: Container(
                                        decoration: BoxDecoration(
                                          border: Border.all(width: borderWidth, color: borderColor),
                                        ),
                                        child: CarouselSlider(
                                          options: CarouselOptions(
                                            height: 320.0,
                                            viewportFraction: 0.7,
                                            enlargeCenterPage: true,
                                          ),
                                          items: feedModel.lstImagePath!.map((filePath) {
                                            return Builder(
                                              builder: (BuildContext context) {
                                                return Container(
                                                  decoration: BoxDecoration(
                                                    border: Border.all(width: borderWidth, color: borderColor),
                                                  ),
                                                  margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      showDialog(
                                                        context: context,
                                                        builder: (_) => showPicture(context, filePath),
                                                      );
                                                    },
                                                    child: CacheImage(
                                                      path: filePath,
                                                      fit: BoxFit.cover,
                                                      borderRadius: 6.0,
                                                    ),
                                                  ),
                                                );
                                              },
                                            );
                                          }).toList(),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                                child: CacheImage(
                                  path: feedModel.lstImagePath![0],
                                  fit: BoxFit.cover,
                                  borderRadius: 5.0,
                                ),
                              ),
                            )
                          : Container(),
                      // PJH, 공백
                      feedModel.lstImagePath!.isNotEmpty
                          ? SizedBox(
                              width: 15,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: borderWidth,
                                    color: borderColor,
                                  ),
                                ),
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ),
                // PJH, 댓글
                Container(
                  decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      // PJH, 공백
                      SizedBox(
                        width: 20,
                      ),
                      // PJH, 날짜
                      Container(
                        alignment: Alignment.centerRight,
                        decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                        child: customText('${feedModel.createdAt.substring(0, 19)}', style: TextStyles.eDetailFeedButton1NormalStyle),
                      ),
                      // PJH, 공백
                      SizedBox(
                        width: 10,
                      ),
                      // PJH, 댓글쓰기
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: borderWidth,
                            color: borderColor,
                          ),
                        ),
                        child: InkWell(
                          onTap: () {
                            var state = Provider.of<FeedState>(context, listen: false);
                            state.setTweetToReply = feedModel;
                            Navigator.of(context).pushNamed('/ComposeTweetPage/');
                          },
                          child: customText(
                            '댓글 쓰기',
                            style: TextStyles.eDetailFeedButton1BoldStyle,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                // PJH, 공백
                SizedBox(
                  height: 12,
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: borderWidth,
                        color: borderColor,
                      ),
                    ),
                  ),
                ),
                // PJH, 라인
                Container(
                  height: 0.6,
                  color: Colors.grey,
                )
              ],
            ),
          ));
    await Future.forEach(feedModel.replyTweetKeyList!, (x) async {
      bool bDone = await addComment(true, x.toString(), lstComment);
    });
    print('[feedPostDetail.dart] addComment() End');
    return true;
  }

  Future<List<Widget>> getComment() async {
    List<Widget> lstComment = [];
    print('[feedPostDetail.dart] getComment() Start');
    FeedModel? feedModel = await getTweetDetail(postId);
    if (feedModel == null) return lstComment;
    await Future.forEach(feedModel.replyTweetKeyList!, (x) async {
      bool bDone = await addComment(false, x.toString(), lstComment);
    });
    print('[feedPostDetail.dart] getComment() End');
    return lstComment;
  }

  Future<FeedModel?> getTweetDetail(String tweetId) async {
    FeedModel _tweetDetail;
    final event = await kDatabase.child('tweet').child(tweetId).once();

    final snapshot = event.snapshot;
    if (snapshot.value != null) {
      var map = snapshot.value as Map<dynamic, dynamic>;
      _tweetDetail = FeedModel.fromJson(map);
      _tweetDetail.key = snapshot.key!;
      return _tweetDetail;
    } else {
      return null;
    }
  }

  Widget _floatingActionButton() {
    return FloatingActionButton(
      backgroundColor: AppColor.primary,
      onPressed: () {
        print('[feedPostDetail.dart] getTweetDetail() PostId : ${postId}');
        var state = Provider.of<FeedState>(context, listen: false);
        state.setTweetToReply = state.tweetDetailModel!.last;
        Navigator.of(context).pushNamed('/ComposeTweetPage/' + postId);
      },
      child: const Icon(Icons.add),
    );
  }

  Widget _tweetDetail(FeedModel model) {
    return Tweet(
      model: model,
      type: TweetType.Detail,
      trailing: TweetBottomSheet().tweetOptionIcon(context, scaffoldKey: scaffoldKey, model: model, type: TweetType.Detail),
      scaffoldKey: scaffoldKey,
    );
  }

  Widget showPicture(context, filePath) {
    // PJH, 테두리
    final borderWidth = 0.1;
    //final borderColor = Colors.red;
    final borderColor = Colors.transparent;

    /*
    return Dialog(
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
              width: borderWidth,
              color: borderColor
          ),
        ),
        child:
        PhotoView(
          imageProvider: customAdvanceNetworkImage(filePath)
        ),
      ),
    );
    */
    return PhotoView(imageProvider: customAdvanceNetworkImage(filePath));
  }

  void openImage() async {
    Navigator.pushNamed(context, '/ImageViewPge');
  }

  @override
  Widget build(BuildContext context) {
    print('[feedPostDetail.dart] build()');
    var state = Provider.of<FeedState>(context);
    return WillPopScope(
        onWillPop: () async {
          Provider.of<FeedState>(context, listen: false).removeLastTweetDetail(postId);
          return Future.value(true);
        },
        child: FutureBuilder(
            future: getComment(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              return Scaffold(
                key: scaffoldKey,
                floatingActionButton: _floatingActionButton(),
                backgroundColor: Theme.of(context).backgroundColor,
                body: CustomScrollView(
                  slivers: [
                    SliverAppBar(
                      pinned: true,
                      iconTheme: IconThemeData(color: AppColor.primary),
                      //backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
                      bottom: PreferredSize(
                        child: Container(
                          color: Colors.grey.shade200,
                          height: 1.0,
                        ),
                        preferredSize: const Size.fromHeight(0.0),
                      ),
                      actions: [
                        IconButton(
                          //icon: Icon(Icons.more_vert),
                          icon: toggleMore ? Icon(Icons.more_horiz) : Icon(Icons.more_vert),
                          onPressed: () async {
                            setState(() {
                              toggleMore = true;
                              print('[feedPostDetail.dart] build() toggleMore : ${toggleMore}');
                            });
                            var selectedId = await showCupertinoModalPopup(
                                context: context,
                                builder: (BuildContext context) => CupertinoActionSheet(
                                  actions: [
                                    CupertinoActionSheetAction(
                                      isDefaultAction: true,
                                      child: Text('삭제', style: TextStyle(color: AppColor.primary)),
                                      onPressed: () async {
                                        print('삭제');
                                        Navigator.pop(context);
                                        var state = Provider.of<FeedState>(context, listen: false);
                                        print('[feedPostDetail.dart] CupertinoActionSheetAction() Step 1');
                                        FeedModel? feedModel = await state.deleteOnlyParent(widget.postId);
                                        if (feedModel != null) {
                                          state.clearAllDetailAndReplyTweetStack();
                                          Navigator.pop(context);
                                          print('[feedPostDetail.dart] CupertinoActionSheetAction() Step 2');
                                          await Future.forEach(feedModel.replyTweetKeyList!, (x) async {
                                            FeedModel? subModel = await getTweetDetail(x.toString());
                                            if (subModel != null && subModel.key != null) {
                                              await state.deleteParent(subModel.key!);
                                              print('[feedPostDetail.dart] CupertinoActionSheetAction() Step 3');
                                            }
                                          });
                                        }
                                        print('[feedPostDetail.dart] CupertinoActionSheetAction() Step 4');
                                      },
                                    ),
                                    CupertinoActionSheetAction(
                                      isDefaultAction: true,
                                      child: Text('수정', style: TextStyle(color: AppColor.primary),),
                                      onPressed: () {
                                        print('수정');
                                        Navigator.pop(context);
                                        // 이동
                                        var state = Provider.of<FeedState>(context, listen: false);
                                        state.setTweetToReply = state.tweetDetailModel!.last;
                                        Navigator.of(context).pushNamed('/ModifyTweet/');
                                      },
                                    ),
                                  ],
                                )
                            );
                            setState(() {
                              toggleMore = false;
                              print('[feedPostDetail.dart] build() Selected ID : ${selectedId}');
                            });
                          },
                        ),
                      ],
                    ),
                    SliverList(
                      delegate: SliverChildListDelegate(
                        [
                          state.tweetDetailModel == null || state.tweetDetailModel!.isEmpty ? Container() : _tweetDetail(state.tweetDetailModel!.last),
                          Container(
                            height: 6,
                            width: context.width,
                            color: TwitterColor.mystic,
                          ),
                        ],
                      ),
                    ),
                    SliverList(
                      delegate: SliverChildListDelegate(state.tweetReplyMap == null || state.tweetReplyMap!.isEmpty || state.tweetReplyMap![postId] == null
                          ? [const Center()]
                          : snapshot.connectionState == ConnectionState.done
                              ? snapshot.data
                              : [const Center()]),
                    )
                  ],
                ),
              );
            }));
  }
}
