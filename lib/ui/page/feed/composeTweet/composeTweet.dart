import 'dart:convert';
import 'dart:io';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_twitter_clone/helper/constant.dart';
import 'package:flutter_twitter_clone/helper/utility.dart';
import 'package:flutter_twitter_clone/model/feedModel.dart';
import 'package:flutter_twitter_clone/model/user.dart';
import 'package:flutter_twitter_clone/ui/page/feed/composeTweet/state/composeTweetState.dart';
import 'package:flutter_twitter_clone/ui/page/feed/composeTweet/widget/composeBottomIconWidget.dart';
import 'package:flutter_twitter_clone/ui/page/feed/composeTweet/widget/composeTweetImage.dart';
import 'package:flutter_twitter_clone/ui/page/feed/composeTweet/widget/widgetView.dart';
import 'package:flutter_twitter_clone/state/authState.dart';
import 'package:flutter_twitter_clone/state/feedState.dart';
import 'package:flutter_twitter_clone/state/searchState.dart';
import 'package:flutter_twitter_clone/ui/page/profile/widgets/circular_image.dart';
import 'package:flutter_twitter_clone/ui/theme/theme.dart';
import 'package:flutter_twitter_clone/widgets/customAppBar.dart';
import 'package:flutter_twitter_clone/widgets/customWidgets.dart';
import 'package:flutter_twitter_clone/widgets/url_text/customUrlText.dart';
import 'package:flutter_twitter_clone/widgets/newWidget/title_text.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:translator/translator.dart';

class ComposeTweetPage extends StatefulWidget {
  const ComposeTweetPage({
    Key? key,
    required this.isRetweet,
    this.isTweet = true,
    this.isModify = false,
  }) : super(key: key);
  final bool isModify;
  final bool isRetweet;
  final bool isTweet;
  @override
  _ComposeTweetReplyPageState createState() => _ComposeTweetReplyPageState();
}

class _ComposeTweetReplyPageState extends State<ComposeTweetPage> {
  File? _image;
  bool isScrollingDown = false;

  // PJH, 테두리
  final borderWidth = 0.1;
  //final borderColor = Colors.red;
  final borderColor = Colors.transparent;

  late FeedModel? model;
  late ScrollController scrollcontroller;
  late TextEditingController _textEditingController;

  String feedTitle = '';
  String feedContent = '';
  String feedStore = '';

  List<File?> lstImage = [];
  List<Widget> lstPicture = [];
  List<String> lstStore = [
    '충무로2가점',
    '화양가중점',
    '기상점',
    '의정가능점',
    '천안메가박스점',
    '동탄중심상가점',
    '통영희가로점',
  ];

  Map<String, Widget> mapPicture = {};
  Map<String, File?> mapFile = {};

  @override
  void dispose() {
    scrollcontroller.dispose();
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    var feedState = Provider.of<FeedState>(context, listen: false);
    model = feedState.tweetToReplyModel;
    _textEditingController = TextEditingController();
    scrollcontroller = ScrollController();
    scrollcontroller.addListener(_scrollListener);

    super.initState();

    print('[composeTweet.dart] initState()');
    print('[composeTweet.dart] initState() isModify : ${widget.isModify}');
    print('[composeTweet.dart] initState() isTweet : ${widget.isTweet}');
    print('[composeTweet.dart] initState() isRetweet : ${widget.isRetweet}');

    // PJH, 점포 가져오기
    kDatabase.child("e24_store").once().then((DatabaseEvent event) async {
      final snapshot = event.snapshot;
      if (snapshot.exists && snapshot.value != null) {
        final mapStore = snapshot.value as Map<dynamic, dynamic>?;
        if (mapStore != null) {
          mapStore.forEach((k, v) {
            setState(() {
              // 메모리 누수 발생 여지 (수정필요)
              lstStore.add(v['str_nm']);
            });
          });
        }
      }
    });

    if (WidgetsBinding.instance != null) {
      WidgetsBinding.instance?.addPostFrameCallback((_) async {
        // PJH, 사진 불러오기
        if (widget.isModify) {
          if (model != null && model!.lstImagePath != null) {
            model!.lstImagePath!.forEach((x) {
              addPicture(File(x));
            });
          }
        }
      });
    }
  }

  void _scrollListener() {
    if (scrollcontroller.position.userScrollDirection == ScrollDirection.reverse) {
      if (!isScrollingDown) {
        Provider.of<ComposeTweetState>(context, listen: false).setIsScrolllingDown = true;
      }
    }
    if (scrollcontroller.position.userScrollDirection == ScrollDirection.forward) {
      Provider.of<ComposeTweetState>(context, listen: false).setIsScrolllingDown = false;
    }
  }

  void _onCrossIconPressed() {
    setState(() {
      _image = null;
    });
  }

  void _onImageIconSelcted(File file) {
    setState(() {
      _image = file;
      addPicture(file);
    });
  }

  void _submitButtonForModify() async {
    if (model == null) {
      return;
    }
    var state = Provider.of<FeedState>(context, listen: false);
    print('[composeTweet.dart] _submitButtonForModify() Step 1');

    state.isBusy = true;
    kScreenloader.showLoader(context);

    // 이미지가 있는 경우
    if (mapFile.isNotEmpty) {
      // PJH, 신규 사진 업로드
      await Future.forEach(mapFile.values, (File? file) async {
        if (file != null) {
          String filePath = file.path;
          print('[composeTweet.dart] _submitButtonForModify() Step 2');
          print('[composeTweet.dart] _submitButtonForModify() File Path : ${filePath}');
          if (!filePath.contains("https")) {
            String? fileURL = await state.uploadFile(file);
            if (fileURL != null) {
              setState(() {
                model!.lstImagePath!.add(fileURL);
                print('[composeTweet.dart] _submitButtonForModify() Step 3');
                print('[composeTweet.dart] _submitButtonForModify() File URL : ${fileURL}');
              });
            }
          }
        }
      });
    }
    // 이미지가 없는 경우
    else {}

    state.updateTweet(model!);
    print('[composeTweet.dart] _submitButtonForModify() Step 4');

    state.isBusy = false;
    kScreenloader.hideLoader();
    print('[composeTweet.dart] _submitButtonForModify() Step 5');

    Navigator.pop(context);
  }

  void _submitButtonForCreate() async {
    if (_textEditingController.text.isEmpty) {
      return;
    }

    var state = Provider.of<FeedState>(context, listen: false);
    kScreenloader.showLoader(context);

    // PJH, FeedModel 생성
    FeedModel tweetModel = await createTweetModel();
    String? tweetId;

    /*
    // PJH, 이미지 있는 경우 v1
    if (_image != null) {
      await state.uploadFile(_image!).then((imagePath) async {
        if (imagePath != null) {
          tweetModel.imagePath = imagePath;

          if (widget.isTweet) {
            tweetId = await state.createTweet(tweetModel);
          }
          else if (widget.isRetweet) {
            tweetId = await state.createReTweet(tweetModel);
          }
          else {
            tweetId = await state.addcommentToPost(tweetModel);
          }
        }
      });
    }
    */

    // PJH, 이미지 있는 경우 v2
    if (mapFile.isNotEmpty) {
      print('[composeTweet.dart] _submitButton() Upload Start');
      await Future.forEach(mapFile.values, (File? file) async {
        String? filePath = await state.uploadFile(file!);
        if (filePath != null) {
          tweetModel.lstImagePath!.add(filePath);
        }
      });
      print('[composeTweet.dart] _submitButton() Upload End');
      print('[composeTweet.dart] _submitButton() Create Tweet Model Start');
      if (widget.isTweet) {
        tweetId = await state.createTweet(tweetModel);
      } else if (widget.isRetweet) {
        tweetId = await state.createReTweet(tweetModel);
      } else {
        tweetId = await state.addcommentToPost(tweetModel);
      }
      print('[composeTweet.dart] _submitButton() Create Tweet Model End');
    }
    // PJH, 이미지 없는 경우
    else {
      // Eweet
      if (widget.isTweet) {
        tweetId = await state.createTweet(tweetModel);
      }
      // ReEweet
      else if (widget.isRetweet) {
        tweetId = await state.createReTweet(tweetModel);
      }
      // Comment
      else {
        tweetId = await state.addcommentToPost(tweetModel);
      }
    }
    tweetModel.key = tweetId;

    await Provider.of<ComposeTweetState>(context, listen: false).sendNotification(tweetModel, Provider.of<SearchState>(context, listen: false)).then((_) {
      kScreenloader.hideLoader();
      Navigator.pop(context);
    });
  }

  void delPicture(File file) {
    print('[composeTweet.dart] delPicture() Path : ${file.path}');
    var isUrl = file.path.contains('https://');
    if (isUrl) {
      model!.lstImagePath!.remove(file.path);
    }
    setState(() {
      mapFile.remove(file.toString());
      mapPicture.remove(file.toString());
      if (mapPicture.isEmpty) {
        _image = null;
      }
    });
  }

  void addPicture(File file) {
    print('[composeTweet.dart] addPicture() Path : ${file.path}');
    var isUrl = file.path.contains('https://');
    setState(() {
      mapFile[file.toString()] = file;
      mapPicture[file.toString()] = Container(
          decoration: BoxDecoration(
            border: Border.all(width: borderWidth, color: borderColor),
          ),
          height: 220.0,
          margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
          child: Stack(
            children: [
              // 사진
              InteractiveViewer(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.black12,
                    borderRadius: BorderRadius.all(Radius.circular(10)),
                    border: Border.all(width: borderWidth, color: borderColor),
                    image: isUrl ? DecorationImage(image: NetworkImage(file.path), fit: BoxFit.cover) : DecorationImage(image: FileImage(file), fit: BoxFit.cover),
                  ),
                ),
              ),
              // 버튼
              Align(
                alignment: Alignment.topRight,
                child: Container(
                    decoration: const BoxDecoration(shape: BoxShape.circle, color: Colors.black54),
                    margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
                    height: 15.0,
                    child: IconButton(
                      onPressed: () {
                        delPicture(file);
                      },
                      iconSize: 15.0,
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      icon: Icon(
                        Icons.close,
                        color: Theme.of(context).colorScheme.onPrimary,
                      ),
                    )),
              ),
            ],
          ));
    });
  }

  Future<FeedModel> createTweetModel() async {
    var state = Provider.of<FeedState>(context, listen: false);
    var authState = Provider.of<AuthState>(context, listen: false);
    var myUser = authState.userModel;
    var profilePic = myUser!.profilePic ?? Constants.dummyProfilePic;

    /// User who are creting reply tweet
    var commentedUser = UserModel(
        displayName: myUser.displayName ?? myUser.email!.split('@')[0],
        profilePic: profilePic,
        userId: myUser.userId,
        isVerified: authState.userModel!.isVerified,
        userName: authState.userModel!.userName,
        empNo: myUser.empNo,
        empNm: myUser.empNm,
        orgCd: myUser.orgCd,
        dutyCd: myUser.dutyCd,
        dutyNm: myUser.dutyNm,
        orgNm: myUser.orgNm,
        superOrgCd: myUser.superOrgCd,
        superOrgNm: myUser.superOrgNm);
    var tags = Utility.getHashTags(_textEditingController.text);
    FeedModel reply = FeedModel(
      description: _textEditingController.text,
      lanCode: (await GoogleTranslator().translate(_textEditingController.text)).sourceLanguage.code,
      user: commentedUser,
      createdAt: DateTime.now().toUtc().toString(),
      tags: tags,
      parentkey: widget.isTweet
          ? null
          : widget.isRetweet
              ? null
              : state.tweetToReplyModel!.key,
      childRetwetkey: widget.isTweet
          ? null
          : widget.isRetweet
              ? model!.key
              : null,
      userId: myUser.userId!,
      // Feed 정보
      feedStore: feedStore,
      feedTitle: feedTitle,
      feedContent: _textEditingController.text,
      lstImagePath: [],
    );
    return reply;
  }

  // PJH, 신규 게시글 작성 화면
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        title: customTitleText(''),
        onActionPressed: widget.isModify ? _submitButtonForModify : _submitButtonForCreate,
        isCrossButton: true,
        submitButtonText: widget.isModify ? '수정' : '등록',
        isSubmitDisable: !Provider.of<ComposeTweetState>(context).enableSubmitButton || Provider.of<FeedState>(context).isBusy,
        isbootomLine: Provider.of<ComposeTweetState>(context).isScrollingDown,
      ),
      backgroundColor: Theme.of(context).backgroundColor,
      body: Stack(
        //!Removed container
        children: [
          SingleChildScrollView(
            controller: scrollcontroller,
            child: widget.isRetweet ? _ComposeReEweet(this) : _ComposeEweet(this),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: ComposeBottomIconWidget(
              textEditingController: _textEditingController,
              onImageIconSelcted: _onImageIconSelcted,
            ),
          ),
        ],
      ),
    );
  }
}

class _ComposeReEweet extends WidgetView<ComposeTweetPage, _ComposeTweetReplyPageState> {
  const _ComposeReEweet(this.viewState) : super(viewState);

  final _ComposeTweetReplyPageState viewState;
  Widget _tweet(BuildContext context, FeedModel model) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        // SizedBox(width: 10),

        const SizedBox(width: 20),
        SizedBox(
          width: context.width - 12,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  SizedBox(
                    width: 25,
                    height: 25,
                    child: CircularImage(path: model.user!.profilePic),
                  ),
                  const SizedBox(width: 10),
                  ConstrainedBox(
                    constraints: BoxConstraints(minWidth: 0, maxWidth: context.width * .5),
                    child: TitleText(model.user!.displayName!, fontSize: 16, fontWeight: FontWeight.w800, overflow: TextOverflow.ellipsis),
                  ),
                  const SizedBox(width: 3),
                  model.user!.isVerified!
                      ? customIcon(
                          context,
                          icon: AppIcon.blueTick,
                          istwitterIcon: true,
                          iconColor: AppColor.primary,
                          size: 13,
                          paddingIcon: 3,
                        )
                      : const SizedBox(width: 0),
                  SizedBox(width: model.user!.isVerified! ? 5 : 0),
                  Flexible(
                    child: customText(
                      '${model.user!.userName}',
                      style: TextStyles.userNameStyle,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  const SizedBox(width: 4),
                  customText('· ${Utility.getChatTime(model.createdAt)}', style: TextStyles.userNameStyle),
                  const Expanded(child: SizedBox()),
                ],
              ),
            ],
          ),
        ),
        if (model.description != null)
          UrlText(
            text: model.description!,
            style: const TextStyle(
              color: Colors.black,
              fontSize: 14,
              fontWeight: FontWeight.w400,
            ),
            urlStyle: const TextStyle(color: Colors.blue, fontWeight: FontWeight.w400),
          ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    var authState = Provider.of<AuthState>(context);
    return SizedBox(
      height: context.height,
      child: Column(
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                child: CircularImage(path: authState.user?.photoURL, height: 40),
              ),
              Expanded(
                child: _TextField(
                  isTweet: false,
                  isRetweet: true,
                  textEditingController: viewState._textEditingController,
                ),
              ),
              const SizedBox(
                width: 16,
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16, left: 80, bottom: 8),
            child: ComposeTweetImage(
              image: viewState._image,
              onCrossIconPressed: viewState._onCrossIconPressed,
            ),
          ),
          Flexible(
            child: Stack(
              children: <Widget>[
                Wrap(
                  children: <Widget>[
                    Container(
                      margin: const EdgeInsets.only(left: 75, right: 16, bottom: 16),
                      padding: const EdgeInsets.all(8),
                      alignment: Alignment.topCenter,
                      decoration: BoxDecoration(border: Border.all(color: AppColor.extraLightGrey, width: .5), borderRadius: const BorderRadius.all(Radius.circular(15))),
                      child: _tweet(context, viewState.model!),
                    ),
                  ],
                ),
                _UserList(
                  list: Provider.of<SearchState>(context).userlist,
                  textEditingController: viewState._textEditingController,
                )
              ],
            ),
          ),
          const SizedBox(height: 50)
        ],
      ),
    );
  }
}

class _ComposeEweet extends WidgetView<ComposeTweetPage, _ComposeTweetReplyPageState> {
  const _ComposeEweet(this.viewState) : super(viewState);
  final _ComposeTweetReplyPageState viewState;

  // PJH, 테두리
  final borderWidth = 0.1;
  //final borderColor = Colors.red;
  final borderColor = Colors.transparent;

  @override
  Widget build(BuildContext context) {
    // PJH, 테두리
    final borderWidth = 0.1;
    //final borderColor = Colors.red;
    final borderColor = Colors.transparent;

    var authState = Provider.of<AuthState>(context, listen: false);

    if (widget.isModify) {
      return widget.isTweet || widget.isRetweet
          ?
          // 게시글 수정
          SingleChildScrollView(
              child: Container(
                decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                child: Column(
                  children: [
                    // 점포
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(width: borderWidth, color: borderColor),
                      ),
                      alignment: Alignment.centerLeft,
                      child: customText(
                        '점포',
                        style: TextStyles.eEweetTitle1NormalStyle,
                      ),
                    ),
                    Container(
                        decoration: BoxDecoration(
                          border: Border.all(width: borderWidth, color: borderColor),
                        ),
                        alignment: Alignment.topLeft,
                        child: Autocomplete<String>(
                          optionsBuilder: (TextEditingValue textEditingValue) {
                            if (textEditingValue.text == '') {
                              return const Iterable<String>.empty();
                            }
                            const List<String> _kOptions = <String>[
                              '충무로2가점',
                              '화양가중점',
                              '기상점',
                              '의정가능점',
                              '천안메가박스점',
                              '동탄중심상가점',
                              '통영희가로점',
                            ];
                            return _kOptions.where((String option) {
                              return option.contains(textEditingValue.text.toLowerCase());
                            });
                          },
                          fieldViewBuilder: (BuildContext context, TextEditingController fieldTextEditingController, FocusNode fieldFocusNode, VoidCallback onFieldSubmitted) {
                            if (viewState.model != null && viewState.model!.feedStore! != '') {
                              fieldTextEditingController..text = viewState.model!.feedStore!;
                            }
                            return Container(
                                decoration: BoxDecoration(
                                  border: Border.all(width: borderWidth, color: borderColor),
                                ),
                                child: TextField(
                                  decoration: InputDecoration(
                                    hintText: '점포를 입력하세요.',
                                    hintStyle: TextStyles.eEweetStoreHintStyle,
                                  ),
                                  controller: fieldTextEditingController,
                                  focusNode: fieldFocusNode,
                                  style: const TextStyle(fontWeight: FontWeight.normal),
                                  onChanged: (text) {
                                    if (viewState.model != null) {
                                      viewState.model!.feedStore = text;
                                    }
                                  },
                                ));
                          },
                          optionsViewBuilder: (context, onSelected, options) {
                            return Align(
                              alignment: Alignment.topLeft,
                              child: Material(
                                child: ConstrainedBox(
                                  constraints: BoxConstraints(maxHeight: 200.0),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      border: Border.all(width: borderWidth, color: borderColor),
                                    ),
                                    child: ListView.builder(
                                      padding: EdgeInsets.zero,
                                      shrinkWrap: true,
                                      itemExtent: 45.0,
                                      itemCount: options.length,
                                      itemBuilder: (BuildContext context, int index) {
                                        return Container(
                                            decoration: BoxDecoration(
                                              border: Border.all(width: borderWidth, color: borderColor),
                                            ),
                                            alignment: Alignment.centerLeft,
                                            child: GestureDetector(
                                              onTap: () {
                                                onSelected(options.elementAt(index));
                                              },
                                              child: Text(
                                                options.elementAt(index),
                                                style: TextStyles.eEweetStoreListStyle,
                                              ),
                                            ));
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                          onSelected: (String selection) {
                            viewState.feedStore = selection;
                            print('점포 : ${viewState.feedStore}');
                          },
                        )),
                    SizedBox(
                      height: 15,
                    ),
                    // 제목
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(width: borderWidth, color: borderColor),
                      ),
                      alignment: Alignment.centerLeft,
                      child: customText(
                        '제목',
                        style: TextStyles.eEweetTitle1NormalStyle,
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(width: borderWidth, color: borderColor),
                      ),
                      alignment: Alignment.centerLeft,
                      child: TextField(
                        controller: TextEditingController()..text = viewState.model!.feedTitle!,
                        onChanged: (text) {
                          if (viewState.model != null) {
                            viewState.model!.feedTitle = text.toString();
                            print('제목 : ${viewState.model!.feedTitle}');
                          }
                        },
                        decoration: InputDecoration(
                          hintText: '제목을 입력하세요.',
                          hintStyle: TextStyles.eEweetStoreHintStyle,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    // 내용 및 요청사항
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(width: borderWidth, color: borderColor),
                      ),
                      alignment: Alignment.centerLeft,
                      child: customText(
                        '내용 및 요청사항',
                        style: TextStyles.eEweetTitle1NormalStyle,
                      ),
                    ),
                    TextField(
                      maxLines: null,
                      controller: TextEditingController()..text = viewState.model!.feedContent!,
                      onChanged: (text) {
                        if (viewState.model != null) {
                          viewState.model!.feedContent = text.toString();
                          viewState.model!.description = text.toString();
                          print('내용 : ${viewState.model!.feedContent}');
                        }
                      },
                      decoration: InputDecoration(border: InputBorder.none, hintText: '내용 및 요청사항을 입력하세요.', hintStyle: TextStyles.eEweetContentHintStyle),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    // 사진
                    viewState.mapPicture.values.toList().isNotEmpty
                        ? Container(
                            decoration: BoxDecoration(
                              border: Border.all(width: borderWidth, color: borderColor),
                            ),
                            alignment: Alignment.centerLeft,
                            child: customText(
                              '사진',
                              style: TextStyles.eEweetTitle1NormalStyle,
                            ),
                          )
                        : Container(),
                    // 사진 불려오기
                    Column(
                      children: viewState.mapPicture.values.toList(),
                    ),
                    // 공백
                    viewState.mapPicture.values.toList().isNotEmpty
                        ? Container(
                            decoration: BoxDecoration(
                              border: Border.all(width: borderWidth, color: borderColor),
                            ),
                            height: 50.0,
                          )
                        : Container(),
                  ],
                ),
              ),
            )
          :
          // 댓글 수정
          SingleChildScrollView();
    } else {
      return widget.isTweet || widget.isRetweet
          ?
          // 게시글 작성
          SingleChildScrollView(
              child: Container(
                decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                child: Column(
                  children: [
                    // 점포
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(width: borderWidth, color: borderColor),
                      ),
                      alignment: Alignment.centerLeft,
                      child: customText(
                        '점포',
                        style: TextStyles.eEweetTitle1NormalStyle,
                      ),
                    ),
                    Container(
                        decoration: BoxDecoration(
                          border: Border.all(width: borderWidth, color: borderColor),
                        ),
                        alignment: Alignment.topLeft,
                        child: false
                            ? TextField(
                                onChanged: (text) {
                                  viewState.feedStore = text.toString();
                                  print('점포 : ${viewState.feedStore}');
                                },
                                decoration: InputDecoration(
                                  hintText: '점포명을 입력하세요.',
                                  hintStyle: TextStyles.eEweetStoreHintStyle,
                                ),
                              )
                            : Autocomplete<String>(
                                optionsBuilder: (TextEditingValue textEditingValue) {
                                  if (textEditingValue.text == '') {
                                    return const Iterable<String>.empty();
                                  }
                                  List<String> _kOptions = viewState.lstStore;
                                  return _kOptions.where((String option) {
                                    return option.contains(textEditingValue.text.toLowerCase());
                                  });
                                },
                                fieldViewBuilder: (BuildContext context, TextEditingController fieldTextEditingController, FocusNode fieldFocusNode, VoidCallback onFieldSubmitted) {
                                  return Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(width: borderWidth, color: borderColor),
                                      ),
                                      child: TextField(
                                        decoration: InputDecoration(
                                          hintText: '점포를 입력하세요.',
                                          hintStyle: TextStyles.eEweetStoreHintStyle,
                                        ),
                                        controller: fieldTextEditingController,
                                        focusNode: fieldFocusNode,
                                        style: const TextStyle(fontWeight: FontWeight.normal),
                                      ));
                                },
                                optionsViewBuilder: (context, onSelected, options) {
                                  return Align(
                                    alignment: Alignment.topLeft,
                                    child: Material(
                                      child: ConstrainedBox(
                                        constraints: BoxConstraints(maxHeight: 200.0),
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(width: borderWidth, color: borderColor),
                                          ),
                                          child: ListView.builder(
                                            padding: EdgeInsets.zero,
                                            shrinkWrap: true,
                                            itemExtent: 45.0,
                                            itemCount: options.length,
                                            itemBuilder: (BuildContext context, int index) {
                                              return Container(
                                                  decoration: BoxDecoration(
                                                    border: Border.all(width: borderWidth, color: borderColor),
                                                  ),
                                                  alignment: Alignment.centerLeft,
                                                  child: GestureDetector(
                                                    onTap: () {
                                                      onSelected(options.elementAt(index));
                                                    },
                                                    child: Text(
                                                      options.elementAt(index),
                                                      style: TextStyles.eEweetStoreListStyle,
                                                    ),
                                                  ));
                                            },
                                          ),
                                        ),
                                      ),
                                    ),
                                  );
                                },
                                onSelected: (String selection) {
                                  viewState.feedStore = selection;
                                  print('점포 : ${viewState.feedStore}');
                                },
                              )),
                    SizedBox(
                      height: 15,
                    ),
                    // 제목
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(width: borderWidth, color: borderColor),
                      ),
                      alignment: Alignment.centerLeft,
                      child: customText(
                        '제목',
                        style: TextStyles.eEweetTitle1NormalStyle,
                      ),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(width: borderWidth, color: borderColor),
                      ),
                      alignment: Alignment.centerLeft,
                      child: TextField(
                        onChanged: (text) {
                          viewState.feedTitle = text.toString();
                          print('제목 : ${viewState.feedTitle}');
                        },
                        decoration: InputDecoration(
                          hintText: '제목을 입력하세요.',
                          hintStyle: TextStyles.eEweetStoreHintStyle,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    // 내용 및 요청사항
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(width: borderWidth, color: borderColor),
                      ),
                      alignment: Alignment.centerLeft,
                      child: customText(
                        '내용 및 요청사항',
                        style: TextStyles.eEweetTitle1NormalStyle,
                      ),
                    ),
                    _TextField(
                      isTweet: widget.isTweet,
                      textEditingController: viewState._textEditingController,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    // 사진
                    viewState.mapPicture.values.toList().isNotEmpty
                        ? Container(
                            decoration: BoxDecoration(
                              border: Border.all(width: borderWidth, color: borderColor),
                            ),
                            alignment: Alignment.centerLeft,
                            child: customText(
                              '사진',
                              style: TextStyles.eEweetTitle1NormalStyle,
                            ),
                          )
                        : Container(),
                    // 사진 불려오기
                    Column(
                      children: viewState.mapPicture.values.toList(),
                    ),
                    // 공백
                    viewState.mapPicture.values.toList().isNotEmpty
                        ? Container(
                            decoration: BoxDecoration(
                              border: Border.all(width: borderWidth, color: borderColor),
                            ),
                            height: 50.0,
                          )
                        : Container(),
                  ],
                ),
              ),
            )
          :
          // 댓글 작성
          SingleChildScrollView(
              child: Container(
                  decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                  margin: EdgeInsets.fromLTRB(10, 0, 10, 10),
                  child: Column(
                    children: [
                      // 댓글
                      Container(
                        decoration: BoxDecoration(
                          border: Border.all(width: borderWidth, color: borderColor),
                        ),
                        alignment: Alignment.centerLeft,
                        child: customText(
                          '댓글',
                          style: TextStyles.eEweetTitle1NormalStyle,
                        ),
                      ),
                      _TextField(
                        isTweet: widget.isTweet,
                        textEditingController: viewState._textEditingController,
                      ),
                      // 사진
                      viewState.mapPicture.values.toList().isNotEmpty
                          ? Container(
                              decoration: BoxDecoration(
                                border: Border.all(width: borderWidth, color: borderColor),
                              ),
                              alignment: Alignment.centerLeft,
                              child: customText(
                                '사진',
                                style: TextStyles.eEweetTitle1NormalStyle,
                              ),
                            )
                          : Container(),
                      /*
            // 사진 v1
            Container(
              decoration: BoxDecoration(
                border: Border.all(
                    width: borderWidth,
                    color: borderColor
                ),
              ),
              alignment: Alignment.center,
              child: ComposeTweetImage(
                image: viewState._image,
                onCrossIconPressed: viewState._onCrossIconPressed,
              ),
            ),
            */
                      // 사진 v2
                      Column(
                        children: viewState.mapPicture.values.toList(),
                      ),
                      // 공백
                      viewState.mapPicture.values.toList().isNotEmpty
                          ? Container(
                              decoration: BoxDecoration(
                                border: Border.all(width: borderWidth, color: borderColor),
                              ),
                              height: 50.0,
                            )
                          : Container(),
                    ],
                  )),
            );
    }
  }
}

class _TextField extends StatelessWidget {
  const _TextField({Key? key, required this.textEditingController, this.isTweet = false, this.isRetweet = false}) : super(key: key);
  final TextEditingController textEditingController;
  final bool isTweet;
  final bool isRetweet;

  @override
  Widget build(BuildContext context) {
    final searchState = Provider.of<SearchState>(context, listen: false);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        TextField(
          controller: textEditingController,
          onChanged: (text) {
            Provider.of<ComposeTweetState>(context, listen: false).onDescriptionChanged(text, searchState);
          },
          maxLines: null,
          decoration: InputDecoration(
              border: InputBorder.none,
              hintText: isTweet
                  ? '내용 및 요청사항을 입력하세요.'
                  : isRetweet
                      ? 'Add a comment'
                      : '내용을 입력하세요.',
              hintStyle: TextStyles.eEweetContentHintStyle),
        ),
      ],
    );
  }
}

class _UserList extends StatelessWidget {
  const _UserList({Key? key, this.list, required this.textEditingController}) : super(key: key);
  final List<UserModel>? list;
  final TextEditingController textEditingController;

  @override
  Widget build(BuildContext context) {
    return !Provider.of<ComposeTweetState>(context).displayUserList || list == null || list!.length < 0 || list!.isEmpty
        ? const SizedBox.shrink()
        : Container(
            padding: const EdgeInsetsDirectional.only(bottom: 50),
            color: TwitterColor.white,
            constraints: const BoxConstraints(minHeight: 30, maxHeight: double.infinity),
            child: ListView.builder(
              itemCount: list!.length,
              physics: ClampingScrollPhysics(),
              itemBuilder: (context, index) {
                return _UserTile(
                  user: list![index],
                  onUserSelected: (user) {
                    textEditingController.text = Provider.of<ComposeTweetState>(context, listen: false).getDescription(user.userName!) + " ";
                    textEditingController.selection = TextSelection.collapsed(offset: textEditingController.text.length);
                    Provider.of<ComposeTweetState>(context, listen: false).onUserSelected();
                  },
                );
              },
            ),
          );
  }
}

class _UserTile extends StatelessWidget {
  const _UserTile({Key? key, required this.user, required this.onUserSelected}) : super(key: key);
  final UserModel user;
  final ValueChanged<UserModel> onUserSelected;

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        onUserSelected(user);
      },
      leading: CircularImage(path: user.profilePic, height: 35),
      title: Row(
        children: <Widget>[
          ConstrainedBox(
            constraints: BoxConstraints(minWidth: 0, maxWidth: context.width * .5),
            child: TitleText(user.displayName!, fontSize: 16, fontWeight: FontWeight.w800, overflow: TextOverflow.ellipsis),
          ),
          const SizedBox(width: 3),
          user.isVerified!
              ? customIcon(
                  context,
                  icon: AppIcon.blueTick,
                  istwitterIcon: true,
                  iconColor: AppColor.primary,
                  size: 13,
                  paddingIcon: 3,
                )
              : const SizedBox(width: 0),
        ],
      ),
      subtitle: Text(user.userName!),
    );
  }
}

class _StoreSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container();
  }
}
