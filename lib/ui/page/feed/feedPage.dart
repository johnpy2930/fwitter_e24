import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_twitter_clone/helper/enum.dart';
import 'package:flutter_twitter_clone/model/feedModel.dart';
import 'package:flutter_twitter_clone/state/authState.dart';
import 'package:flutter_twitter_clone/state/feedState.dart';
import 'package:flutter_twitter_clone/ui/theme/theme.dart';
import 'package:flutter_twitter_clone/widgets/customWidgets.dart';
import 'package:flutter_twitter_clone/widgets/newWidget/customLoader.dart';
import 'package:flutter_twitter_clone/widgets/newWidget/emptyList.dart';
import 'package:flutter_twitter_clone/widgets/tweet/tweet.dart';
import 'package:flutter_twitter_clone/widgets/tweet/widgets/tweetBottomSheet.dart';
import 'package:provider/provider.dart';

class FeedPage extends StatelessWidget {
  const FeedPage({Key? key, required this.scaffoldKey, this.refreshIndicatorKey}) : super(key: key);
  final GlobalKey<ScaffoldState> scaffoldKey;
  final GlobalKey<RefreshIndicatorState>? refreshIndicatorKey;

  Widget _floatingActionButton(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: AppColor.primary,
      onPressed: () {
        Navigator.of(context).pushNamed('/CreateFeedPage/tweet');
      },
      child: customIcon(
        context,
        icon: AppIcon.fabTweet,
        istwitterIcon: true,
        iconColor: Theme.of(context).colorScheme.onPrimary,
        size: 25,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: _floatingActionButton(context),
      backgroundColor: TwitterColor.mystic,
      body: SafeArea(
        child: SizedBox(
          height: context.height,
          width: context.width,
          child: RefreshIndicator(
            key: refreshIndicatorKey,
            onRefresh: () async {
              /// refresh home page feed
              var feedState = Provider.of<FeedState>(context, listen: false);
              feedState.getDataFromDatabase();
              return Future.value(true);
            },
            child: _FeedPageBody(
              refreshIndicatorKey: refreshIndicatorKey,
              scaffoldKey: scaffoldKey,
            ),
          ),
        ),
      ),
    );
  }
}

class _FeedPageBody extends StatefulWidget {
  const _FeedPageBody({Key? key, required this.scaffoldKey, this.refreshIndicatorKey}) : super(key: key);
  final GlobalKey<ScaffoldState> scaffoldKey;
  final GlobalKey<RefreshIndicatorState>? refreshIndicatorKey;

  @override
  _FeedPageBodyState createState() => _FeedPageBodyState();

  /*
  // 기존 로직
  @override
  Widget build(BuildContext context) {
    var authstate = Provider.of<AuthState>(context, listen: false);
    return Consumer<FeedState>(
      builder: (context, state, child) {
        final List<FeedModel>? list = state.getTweetList(authstate.userModel);
        return CustomScrollView(
          slivers: <Widget>[
            child!,
            state.isBusy && list == null
                ? SliverToBoxAdapter(
                    child: SizedBox(
                      height: context.height - 135,
                      child: CustomScreenLoader(
                        height: double.infinity,
                        width: context.width,
                        backgroundColor: Colors.white,
                      ),
                    ),
                  )
                : !state.isBusy && list == null
                    ? const SliverToBoxAdapter(
                        child: EmptyList(
                          'No Tweet added yet',
                          subTitle:
                              'When new Tweet added, they\'ll show up here \n Tap tweet button to add new',
                        ),
                      )
                    : SliverList(
                        delegate: SliverChildListDelegate(
                          list!.map(
                            (model) {
                              return Container(
                                color: Colors.white,
                                child: Tweet(
                                  model: model,
                                  trailing: TweetBottomSheet().tweetOptionIcon(
                                      context,
                                      model: model,
                                      type: TweetType.Tweet,
                                      scaffoldKey: scaffoldKey),
                                  scaffoldKey: scaffoldKey,
                                ),
                              );
                            },
                          ).toList(),
                        ),
                      )
          ],
        );
      },
      child: SliverAppBar(
        floating: true,
        elevation: 0,
        leading: Builder(
          builder: (BuildContext context) {
            return IconButton(
              icon: const Icon(Icons.menu),
              onPressed: () {
                scaffoldKey.currentState!.openDrawer();
              },
            );
          },
        ),
        title: Image.asset('assets/images/icon-480.png', height: 40),
        centerTitle: true,
        iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
        backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
        bottom: PreferredSize(
          child: Container(
            color: Colors.grey.shade200,
            height: 1.0,
          ),
          preferredSize: const Size.fromHeight(0.0),
        ),
      ),
    );
  }
  */

  /*
  신규 로직
  @override
  Widget build(BuildContext context) {
    // PJH, 테두리
    final borderWidth = 1.0;
    //final borderColor = Colors.red;
    final borderColor = Colors.transparent;
    var _bAppExpanded = true;
    var _scrollController = ScrollController();
    _scrollController.addListener(() {
      //print('hasClients : ${_scrollController.hasClients}');
      //print('offset : ${_scrollController.offset}');
      //print('Height * 0.25 : ${MediaQuery.of(context).size.height * 0.25}');
      if (_scrollController.hasClients) {
        _bAppExpanded = _scrollController.offset > (MediaQuery.of(context).size.height * 0.25);
      }
      //print('App Expanded : ${_bAppExpanded}');
    });

    return Scaffold(
      body: Consumer<FeedState>(
          builder: (context, state, child) {
            var authstate = Provider.of<AuthState>(context, listen: false);
            final List<FeedModel>? list = state.getTweetList(authstate.userModel);
            return CustomScrollView(
              controller: _scrollController,
              slivers: [
                SliverAppBar(
                  leading: Builder(
                    builder: (BuildContext context) {
                      return IconButton(
                        icon: const Icon(Icons.menu),
                        onPressed: () {
                          scaffoldKey.currentState!.openDrawer();
                        },
                      );
                    },
                  ),
                  title: Image.asset('assets/images/icon-480.png', height: 40),
                  centerTitle: true,
                  pinned: true,
                  iconTheme: IconThemeData(color: Theme.of(context).primaryColor),
                  backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
                  bottom: PreferredSize(
                    child: Container(
                      color: Colors.grey.shade200,
                      height: 1.0,
                    ),
                    preferredSize: const Size.fromHeight(0.0),
                  ),
                  flexibleSpace: FlexibleSpaceBar(
                    background: Image.asset('assets/images/e24_store.jpeg', fit: BoxFit.cover,),
                  ),
                  expandedHeight: MediaQuery.of(context).size.height * 0.25,
                ),
                state.isBusy && list == null ?
                SliverToBoxAdapter(
                  child: SizedBox(
                    height: context.height - 135,
                    child: CustomScreenLoader(
                      height: double.infinity,
                      width: context.width,
                      backgroundColor: Colors.white,
                    ),
                  ),
                ) :
                !state.isBusy && list == null ?
                SliverToBoxAdapter(
                  child: EmptyList(
                    'No Tweet added yet',
                    subTitle:
                    'When new Tweet added, they\'ll show up here \n Tap tweet button to add new',
                  ),
                ) :
                SliverList(
                  delegate: SliverChildListDelegate(
                    list!.map((model) {
                      return Container(
                        color: Colors.white,
                        child: Tweet(
                          model: model,
                          trailing: TweetBottomSheet().tweetOptionIcon(
                              context,
                              model: model,
                              type: TweetType.Tweet,
                              scaffoldKey: scaffoldKey
                          ),
                          scaffoldKey: scaffoldKey,
                        ),
                      );
                    }).toList(),
                  ),
                )
              ],
            );
          }
      ),
    );
  }
  */

}

class _FeedPageBodyState extends State<_FeedPageBody> {
  late GlobalKey<ScaffoldState> scaffoldKey;
  late GlobalKey<RefreshIndicatorState>? refreshIndicatorKey;
  var _scrollController = ScrollController();
  var _appExpaned = false;

  @override
  void initState() {
    scaffoldKey = widget.scaffoldKey;
    refreshIndicatorKey = widget.refreshIndicatorKey;
    super.initState();
    _scrollController.addListener(() {
      print('hasClients : ${_scrollController.hasClients}');
      print('offset : ${_scrollController.offset}');
      print('Height * 0.25 : ${MediaQuery.of(context).size.height * 0.25}');
      if (_scrollController.hasClients) {
        setState(() {
          _appExpaned = _scrollController.offset >= 100.0;
        });
      }
      print('App Expanded Flag : ${_appExpaned}');
    });
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark);
  }

  @override
  Widget build(BuildContext context) {
    // PJH, 테두리
    final borderWidth = 1.0;
    //final borderColor = Colors.red;
    final borderColor = Colors.transparent;
    return Scaffold(
      body: Consumer<FeedState>(builder: (context, state, child) {
        print('[feedPage.dart] Consumer()');
        var authstate = Provider.of<AuthState>(context, listen: false);
        final List<FeedModel>? list = state.getTweetList(authstate.userModel);
        return CustomScrollView(
          controller: _scrollController,
          slivers: [
            SliverAppBar(
              leading: Builder(
                builder: (BuildContext context) {
                  return IconButton(
                    icon: _appExpaned
                        ? const Icon(
                            Icons.menu,
                            color: AppColor.primary,
                          )
                        : const Icon(
                            Icons.menu,
                            color: Colors.white,
                          ),
                    onPressed: () {
                      scaffoldKey.currentState!.openDrawer();
                    },
                  );
                },
              ),
              actions: [
                IconButton(
                  icon: _appExpaned
                      ? const Icon(
                          AppIcon.notification,
                          color: AppColor.primary,
                        )
                      : const Icon(
                          AppIcon.notification,
                          color: Colors.white,
                        ),
                  onPressed: () {
                    print('notification clicked');
                  },
                )
              ],
              title: _appExpaned ? Image.asset('assets/images/icon-480.png', height: 40) : Text(''),
              centerTitle: true,
              pinned: true,
              iconTheme: IconThemeData(color: AppColor.primary),
              backgroundColor: Theme.of(context).appBarTheme.backgroundColor,
              bottom: PreferredSize(
                child: Container(
                  color: Colors.grey.shade200,
                  height: 1.0,
                ),
                preferredSize: const Size.fromHeight(0.0),
              ),
              flexibleSpace: FlexibleSpaceBar(
                background: Image.asset(
                  'assets/images/e24_store.jpeg',
                  fit: BoxFit.cover,
                ),
              ),
              expandedHeight: MediaQuery.of(context).size.height * 0.25,
            ),
            state.isBusy && list == null
                ? SliverToBoxAdapter(
                    child: SizedBox(
                      height: context.height - (MediaQuery.of(context).size.height * 0.5),
                      child: CustomScreenLoader(
                        height: double.infinity,
                        width: context.width,
                        backgroundColor: Colors.white,
                      ),
                    ),
                  )
                : !state.isBusy && list == null
                    ? SliverToBoxAdapter(
                        child: EmptyList(
                          'No Tweet added yet',
                          subTitle: 'When new Tweet added, they\'ll show up here \n Tap tweet button to add new',
                        ),
                      )
                    : SliverList(
                        delegate: SliverChildListDelegate(
                          list!.map((model) {
                            return Container(
                              color: Colors.white,
                              child: Tweet(
                                model: model,
                                trailing: TweetBottomSheet().tweetOptionIcon(context,
                                    model: model, type: TweetType.Tweet, scaffoldKey: scaffoldKey),
                                scaffoldKey: scaffoldKey,
                              ),
                            );
                          }).toList(),
                        ),
                      )
          ],
        );
      }),
    );
  }
}
