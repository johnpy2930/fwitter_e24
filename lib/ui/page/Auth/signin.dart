import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_twitter_clone/helper/utility.dart';
import 'package:flutter_twitter_clone/state/authState.dart';
import 'package:flutter_twitter_clone/ui/page/Auth/widget/googleLoginButton.dart';
import 'package:flutter_twitter_clone/ui/theme/theme.dart';
import 'package:flutter_twitter_clone/widgets/customFlatButton.dart';
import 'package:flutter_twitter_clone/widgets/customWidgets.dart';
import 'package:flutter_twitter_clone/widgets/newWidget/customLoader.dart';
import 'package:provider/provider.dart';

class SignIn extends StatefulWidget {
  final VoidCallback? loginCallback; //!

  const SignIn({Key? key, this.loginCallback}) : super(key: key);
  @override
  State<StatefulWidget> createState() => _SignInState();
}

class _SignInState extends State<SignIn> {
  late TextEditingController _emailController;
  late TextEditingController _passwordController;
  late CustomLoader loader;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    loader = CustomLoader();
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  Widget _body(BuildContext context) {
    // PJH, 테두리
    final borderWidth = 1.0;
    //final borderColor = Colors.red;
    final borderColor = Colors.transparent;

    return SingleChildScrollView(
      child: Container(
          margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
          decoration: BoxDecoration(
            border: Border.all(
              width: borderWidth,
              color: borderColor,
            ),
          ),
          /*
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(height: 40),
          // YEJ, 텍스트 및 이미지 추가
          Text('현장의 소리',
            style: TextStyle(
                color: AppColor.primary,
                fontFamily: 'NotoSansKR',
                fontSize: 25,
                fontWeight: FontWeight.bold
            ),),
          const SizedBox(
            height: 20,
          ),
          Container(
            decoration: BoxDecoration(
              border: Border.all(
                width: borderWidth,
                color: borderColor,
              ),
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(30.0),
              child:  Image.asset('assets/images/e24_store.jpeg',fit: BoxFit.cover),
            ),
          ),
          const SizedBox(
            height: 20,
          ),
          _entryFeild('사번', controller: _emailController),
          _entryFeild('패스워드', controller: _passwordController, isPassword: true),
          _emailLoginButton(context),
          const SizedBox(height: 10),
          _labelButton('Forget password?', onPressed: () {
            Navigator.of(context).pushNamed('/ForgetPasswordPage');
          })
        ],
      ),
      */
          child: Center(
              heightFactor: null,
              widthFactor: null,
              child: Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      width: borderWidth,
                      color: borderColor,
                    ),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Text(
                        '현장의 소리',
                        style: TextStyle(
                            color: AppColor.primary,
                            fontFamily: 'NotoSansKR',
                            fontSize: 25,
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(height: 30),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(30.0),
                        child: Image.asset('assets/images/e24_store.jpeg',
                            fit: BoxFit.cover),
                      ),
                      _entryFeild('사번', controller: _emailController),
                      _entryFeild('패스워드',
                          controller: _passwordController, isPassword: true),
                      _emailLoginButton(context),
                    ],
                  )))),
    );
  }

  Widget _entryFeild(String hint,
      {required TextEditingController controller, bool isPassword = false}) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 12),
      decoration: BoxDecoration(
        color: Colors.grey.shade100,
        borderRadius: BorderRadius.circular(20),
        // YEJ, 그림자 추가
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.2),
            spreadRadius: 2,
            blurRadius: 3,
            offset: Offset(0, 2), // changes position of shadow
          ),
        ],
      ),
      child: TextField(
        controller: controller,
        keyboardType: TextInputType.emailAddress,
        style: const TextStyle(
          fontStyle: FontStyle.normal,
          fontWeight: FontWeight.normal,
        ),
        obscureText: isPassword,
        decoration: InputDecoration(
          hintText: hint,
          border: InputBorder.none,
          focusedBorder: const OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0)),
              borderSide: BorderSide(color: AppColor.primary)),
          contentPadding:
              const EdgeInsets.symmetric(vertical: 15, horizontal: 20),
        ),
      ),
    );
  }

  Widget _labelButton(String title, {Function? onPressed}) {
    return TextButton(
      onPressed: () {
        if (onPressed != null) {
          onPressed();
        }
      },
      child: Text(
        title,
        style: TextStyle(
            color: AppColor.primary,
            fontWeight: FontWeight.normal,
            fontFamily: 'NotoSansKR'),
      ),
    );
  }

  Widget _emailLoginButton(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 30),
      child: CustomFlatButton(
        label: "로그인",
        onPressed: _emailLogin,
        borderRadius: 30,
        color: AppColor.primary,
      ),
    );
  }

  void _emailLogin() {
    var state = Provider.of<AuthState>(context, listen: false);
    if (state.isbusy) {
      return;
    }
    loader.showLoader(context);
    // PJH, 암호 복잡도 확인 제거
    var isValid = true;
    /*
    var isValid = Utility.validateCredentials(
        _scaffoldKey, _emailController.text, _passwordController.text
    );
    */
    if (isValid) {
      state
          .signIn(_emailController.text, _passwordController.text,
              scaffoldKey: _scaffoldKey)
          .then((status) {
        if (state.user != null) {
          loader.hideLoader();
          Navigator.pop(context);
          widget.loginCallback!();
        } else {
          cprint('Unable to login', errorIn: '_emailLoginButton');
          loader.hideLoader();
        }
      });
    } else {
      loader.hideLoader();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      key: _scaffoldKey,
      appBar: AppBar(
        title: customText('Sign in',
            context: context, style: const TextStyle(fontSize: 20)),
        centerTitle: true,
      ),
      body: _body(context),
    );
  }
}
