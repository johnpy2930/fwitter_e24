import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_twitter_clone/helper/constant.dart';
import 'package:flutter_twitter_clone/model/user.dart';

class CircularImage extends StatelessWidget {
  const CircularImage(
      {Key? key,
      this.path,
      this.empNo = null,
      this.height = 50,
      this.isBorder = false,
      this.borderWidth = 2.0,
      this.borderColor = Colors.white})
      : super(key: key);
  final String? empNo;
  final String? path;
  final double height;
  final double borderWidth;
  final bool isBorder;
  final Color borderColor;

  @override
  Widget build(BuildContext context) {
    /*
    // PJH, 프로필 사진 v1
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(
            color: Colors.grey.shade100,
            width: isBorder ? 2 : 0
        ),
      ),
      child: CircleAvatar(
        maxRadius: height / 2,
        backgroundColor: Theme.of(context).cardColor,
        backgroundImage: this.empNo == null ?
        customAdvanceNetworkImage(path ?? Constants.dummyProfilePic) :
        Image.asset('assets/profile/${empNo!}.png').image
      ),
    );
    */
    // PJH, 프로필 사진 v2
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border:
            Border.all(color: borderColor, width: isBorder ? borderWidth : 0.0),
      ),
      child: CircleAvatar(
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: Colors.white, width: 2),
              borderRadius: BorderRadius.circular(height / 2),
              image: DecorationImage(
                  image: this.empNo == null
                      ? customAdvanceNetworkImage(
                          path ?? Constants.dummyProfilePic)
                      : Image.asset('assets/profile/${empNo!}.png').image)),
        ),
      ),
    );
  }
}

CachedNetworkImageProvider customAdvanceNetworkImage(String? path) {
  if (path ==
      'http://www.azembelani.co.za/wp-content/uploads/2016/07/20161014_58006bf6e7079-3.png') {
    path = Constants.dummyProfilePic;
  } else {
    path ??= Constants.dummyProfilePic;
  }
  return CachedNetworkImageProvider(
    path,
    cacheKey: path,
  );
}
