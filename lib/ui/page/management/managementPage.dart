import 'package:flutter/material.dart';
import 'package:flutter_twitter_clone/helper/enum.dart';
import 'package:flutter_twitter_clone/model/feedModel.dart';
import 'package:flutter_twitter_clone/resource/push_notification_service.dart';
import 'package:flutter_twitter_clone/state/managementState.dart';
import 'package:flutter_twitter_clone/state/bookmarkState.dart';
import 'package:flutter_twitter_clone/ui/theme/theme.dart';
import 'package:flutter_twitter_clone/widgets/customAppBar.dart';
import 'package:flutter_twitter_clone/widgets/newWidget/emptyList.dart';
import 'package:flutter_twitter_clone/widgets/tweet/tweet.dart';
import 'package:provider/provider.dart';

import '../common/locator.dart';

class ManagementPage extends StatelessWidget {
  const ManagementPage({Key? key}) : super(key: key);

  static Route<T> getRoute<T>() {
    return MaterialPageRoute(
      builder: (_) {
        return Provider(
          create: (_) => ManagementState(),
          child: ChangeNotifierProvider(
            create: (BuildContext context) => ManagementState(),
            builder: (_, child) => const ManagementPage(),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
        title: Text("Management", style: TextStyles.titleStyle),
        isBackButton: true,
      ),
      body: const ManagementBody(),
    );
  }
}

class ManagementBody extends StatelessWidget {
  const ManagementBody({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    var state = Provider.of<ManagementState>(context, listen: false);
    return SingleChildScrollView(
      child: Container(
        color: Colors.white,
        padding: EdgeInsets.all(10.0),
        child: Column(
          children: [
            Row(
              children: [
                MaterialButton(
                  color: Colors.grey,
                  minWidth: 50.0,
                  child: Text('실행'),
                  onPressed: () {},
                ),
                SizedBox(
                  width: 30,
                ),
                Text('테스트 업로드')
              ],
            ),
            Row(
              children: [
                MaterialButton(
                  color: Colors.grey,
                  minWidth: 50.0,
                  child: Text('실행'),
                  onPressed: () {
                    state.importUser(context);
                  },
                ),
                SizedBox(
                  width: 30,
                ),
                Text('사용자 업로드')
              ],
            ),
            Row(
              children: [
                MaterialButton(
                  color: Colors.grey,
                  minWidth: 50.0,
                  child: Text('실행'),
                  onPressed: () {
                    state.importORG(context);
                  },
                ),
                SizedBox(
                  width: 30,
                ),
                Text('조직 업로드')
              ],
            ),
            Row(
              children: [
                MaterialButton(
                  color: Colors.grey,
                  minWidth: 50.0,
                  child: Text('실행'),
                  onPressed: () {
                    state.importStore(context);
                  },
                ),
                SizedBox(
                  width: 30,
                ),
                Text('점포 업로드')
              ],
            ),
            Row(
              children: [
                MaterialButton(
                  color: Colors.grey,
                  minWidth: 50.0,
                  child: Text('실행'),
                  onPressed: () {},
                ),
                SizedBox(
                  width: 30,
                ),
                Text('점포 로컬 저장')
              ],
            ),
            Row(
              children: [
                MaterialButton(
                  color: Colors.grey,
                  minWidth: 50.0,
                  child: Text('실행'),
                  onPressed: () {},
                ),
                SizedBox(
                  width: 30,
                ),
                Text('점포 로컬 불려오기')
              ],
            ),
            Row(
              children: [
                MaterialButton(
                  color: Colors.grey,
                  minWidth: 50.0,
                  child: Text('실행'),
                  onPressed: () async {
                    String? token =
                        await getIt<PushNotificationService>().getDeviceToken();
                    if (token != null) {
                      print('Token : ${token}');
                    }
                  },
                ),
                SizedBox(
                  width: 30,
                ),
                Text('FCM 토큰 가져오기')
              ],
            ),
          ],
        ),
      ),
    );
  }
}
