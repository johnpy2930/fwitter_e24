import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_twitter_clone/state/settingState.dart';
import 'package:flutter_twitter_clone/widgets/customAppBar.dart';
import 'package:flutter_twitter_clone/widgets/customWidgets.dart';
import 'package:provider/provider.dart';

import '../../../state/profile_state.dart';
import '../../theme/theme.dart';

class SettingPage extends StatefulWidget{
  const SettingPage({Key? key, required this.profileId, required this.isPush}) : super(key: key);

  final String profileId;
  final bool isPush;

  static MaterialPageRoute getRoute({required String profileId, required bool isPush}) {
    return MaterialPageRoute(
      builder: (_) =>
          Provider(
            create: (_) => SettingState(),
            child: ChangeNotifierProvider(
              create: (BuildContext context) => SettingState(),
              builder: (_, child) =>
                  SettingPage(
                    profileId: profileId,
                    isPush: isPush,
                  ),
            ),
          ),
    );
  }

    @override
    _SettingPage createState() => _SettingPage();
}

class _SettingPage extends State<SettingPage>{

  bool isPush = true;

  @override
  void initState() {
    super.initState();
    isPush = widget.isPush;
  }

  @override
  void dispose() {
    //_tabController.dispose();
    super.dispose();
  }

  @override
  build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(
        title: Text(''),
        isBackButton: true,
      ),
      body:
        Padding(
          padding: const EdgeInsets.all(20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 20,),
                Text('푸시알림',style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                SizedBox(height: 15,),
                Row(
                  children: [
                    Text('알림 허용',style: TextStyle(fontSize: 18)),
                    Spacer(),
                    Switch(value: isPush, onChanged: (value) {
                      setState(() {
                        isPush = value;
                        var state = Provider.of<SettingState>(context, listen: false);
                        state.updatePush(widget.profileId, isPush);
                      });
                    },
                      activeColor: AppColor.primary,
                    )
                  ],
                ),
                /*
                SizedBox(height: 10,),
                Text('게시글',style: TextStyle(fontSize: 16)),
                SizedBox(height: 10,),
                Text('댓글',style: TextStyle(fontSize: 16)),
                 */

              ],
            ),
        ),


    );
  }
}