import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_twitter_clone/helper/enum.dart';
import 'package:flutter_twitter_clone/helper/utility.dart';
import 'package:flutter_twitter_clone/model/feedModel.dart';
import 'package:flutter_twitter_clone/state/feedState.dart';
import 'package:flutter_twitter_clone/ui/page/feed/feedPostDetail.dart';
import 'package:flutter_twitter_clone/ui/page/profile/profilePage.dart';
import 'package:flutter_twitter_clone/ui/page/profile/widgets/circular_image.dart';
import 'package:flutter_twitter_clone/ui/theme/theme.dart';
import 'package:flutter_twitter_clone/widgets/newWidget/title_text.dart';
import 'package:flutter_twitter_clone/widgets/tweet/widgets/parentTweet.dart';
import 'package:flutter_twitter_clone/widgets/tweet/widgets/tweetIconsRow.dart';
import 'package:flutter_twitter_clone/widgets/url_text/customUrlText.dart';
import 'package:flutter_twitter_clone/widgets/url_text/custom_link_media_info.dart';
import 'package:flutter_twitter_clone/widgets/cache_image.dart';
import 'package:provider/provider.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:photo_view/photo_view.dart';

import '../../state/authState.dart';
import '../customWidgets.dart';
import 'widgets/retweetWidget.dart';
import 'widgets/tweetImage.dart';

class LikeHateWidget extends StatefulWidget {
  FeedModel model;
  LikeHateWidget(this.model);
  @override
  LikeHateState createState() => LikeHateState();
}

class LikeHateState extends State<LikeHateWidget> {
  bool bLike = false;
  bool bHate = false;
  late var feedState;
  late var authState;

  @override
  void initState() {
    print('[tweet.dart] initState()');
    super.initState();
    feedState = Provider.of<FeedState>(context, listen: false);
    authState = Provider.of<AuthState>(context, listen: false);

    if (widget.model.likeList != null) {
      bLike = widget.model.likeList!.contains(authState.userId);
    }
    if (widget.model.hateList != null) {
      bHate = widget.model.hateList!.contains(authState.userId);
    }
  }

  @override
  void deactivate() {
    print('[tweet.dart] deactivate()');
    super.deactivate();
  }

  @override
  void dispose() {
    print('[tweet.dart] dispose()');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // YEJ, 추천 및 비추천 버튼
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // 추천
        Container(
          width: 70,
          height: 40,
          decoration: BoxDecoration(color: AppColor.primary, borderRadius: BorderRadius.circular(10)),
          child: ElevatedButton.icon(
            onPressed: () {
              setState(() {
                bLike = !bLike;
                feedState.addLikeToTweet(widget.model, authState.userId);
              });
            },
            icon: Icon(
              Icons.thumb_up_outlined,
              // Icon Color
              color: !bLike ? AppColor.primary : Colors.white,
              size: 20,
            ),
            label: Text('${widget.model.likeCount ?? 0}'),
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              // Background Color
              primary: !bLike ? Colors.white : AppColor.primary,
              // Text Color
              onPrimary: !bLike ? AppColor.primary : Colors.white,
            ),
          ),
        ),
        const SizedBox(
          width: 30,
        ),
        // 비추천
        Container(
          width: 70,
          height: 40,
          decoration: BoxDecoration(color: AppColor.primary, borderRadius: BorderRadius.circular(10)),
          child: ElevatedButton.icon(
            onPressed: () {
              setState(() {
                bHate = !bHate;
                feedState.addHateToTweet(widget.model, authState.userId);
              });
            },
            icon: Icon(
              Icons.thumb_down_outlined,
              // Icon Color
              color: !bHate ? AppColor.primary : Colors.white,
              size: 20,
            ),
            label: Text('${widget.model.hateCount ?? 0}'),
            style: ElevatedButton.styleFrom(
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
              // Background Color
              primary: !bHate ? Colors.white : AppColor.primary,
              // Text Color
              onPrimary: !bHate ? AppColor.primary : Colors.white,
            ),
          ),
        )
        // /AppIcon
      ],
    );
  }
}

class Tweet extends StatelessWidget {
  final FeedModel model;
  final Widget? trailing;
  final TweetType type;
  final bool isDisplayOnProfile;
  final GlobalKey<ScaffoldState> scaffoldKey;
  const Tweet({
    Key? key,
    required this.model,
    this.trailing,
    this.type = TweetType.Tweet,
    this.isDisplayOnProfile = false,
    required this.scaffoldKey,
  }) : super(key: key);

  void onLongPressedTweet(BuildContext context) {
    if (type == TweetType.Detail || type == TweetType.ParentTweet) {
      Utility.copyToClipBoard(scaffoldKey: scaffoldKey, text: model.description ?? "", message: "Tweet copy to clipboard");
    }
  }

  void onTapTweet(BuildContext context) {
    var feedstate = Provider.of<FeedState>(context, listen: false);
    if (type == TweetType.Detail || type == TweetType.ParentTweet) {
      return;
    }
    if (type == TweetType.Tweet && !isDisplayOnProfile) {
      feedstate.clearAllDetailAndReplyTweetStack();
    }
    feedstate.getpostDetailFromDatabase(null, model: model);
    Navigator.push(context, FeedPostDetail.getRoute(model.key!));

    if (type == TweetType.Reply) {
      print('Reply');
    }

    if (type == TweetType.Detail) {
      print('Detail');
    }
  }

  // PJH, 신규 Tweet 레이아웃
  @override
  Widget build(BuildContext context) {
    // PJH, 테두리
    final borderWidth = 0.1;
    //final borderColor = Colors.red;
    final borderColor = Colors.transparent;

    // PJH, 댓글
    final commentSize = 20.0;
    final commentCountSize = 25.0;
    final commentSpaceWidth = 5.0;

    return InkWell(
        onLongPress: () {},
        onTap: () {
          onTapTweet(context);
        },
        child: Container(
            decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
            child:
                // PJH, Tweet 형식
                type == TweetType.Tweet
                    ? Container(
                        decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                        child: Column(
                          children: [
                            // PJH, 공백
                            SizedBox(
                              height: 12,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: borderWidth,
                                    color: borderColor,
                                  ),
                                ),
                              ),
                            ),
                            // PJH, 프로필, 이름, 조직, 날짜
                            Container(
                              decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  // PJH, 프로필, 이름, 조직 (왼쪽)
                                  Container(
                                    decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      children: [
                                        // PJH, 공백
                                        SizedBox(
                                          width: 15,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                width: borderWidth,
                                                color: borderColor,
                                              ),
                                            ),
                                          ),
                                        ),
                                        // PJH, 프로필
                                        SizedBox(
                                          width: 42,
                                          height: 42,
                                          child: Container(
                                            child: CircularImage(
                                              path: model.user!.profilePic,
                                              empNo: model.user!.empNo,
                                              isBorder: true,
                                              borderWidth: 0.5,
                                              borderColor: Colors.grey,
                                            ),
                                          ),
                                        ),
                                        // PJH, 공백
                                        SizedBox(
                                          width: 5,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                width: borderWidth,
                                                color: borderColor,
                                              ),
                                            ),
                                          ),
                                        ),
                                        // PJH, 이름
                                        SizedBox(
                                          child: Container(
                                            child: customText(
                                              '${model.user!.displayName}',
                                              style: TextStyles.eFeedNameStyle,
                                            ),
                                          ),
                                        ),
                                        // PJH, 공백
                                        SizedBox(
                                          width: 5,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                width: borderWidth,
                                                color: borderColor,
                                              ),
                                            ),
                                          ),
                                        ),
                                        // PJH, 조직도
                                        SizedBox(
                                          child: Container(
                                            child: customText(
                                              '${model.user!.orgNm}',
                                              style: TextStyles.eFeedOrgStyle,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  // PJH, 날짜 (오른쪽)
                                  Expanded(
                                    child: Container(
                                      alignment: Alignment.centerRight,
                                      decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        children: [
                                          // PJH, 날짜
                                          customText('${model.createdAt.substring(0, 19)}', style: TextStyles.eFeedDateStyle),
                                          // PJH, 공백
                                          SizedBox(
                                            width: 10,
                                            child: Container(
                                              decoration: BoxDecoration(
                                                border: Border.all(
                                                  width: borderWidth,
                                                  color: borderColor,
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            // PJH, 제목 & 사진
                            Container(
                              decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                              child: Row(
                                children: [
                                  // PJH, 공백
                                  SizedBox(
                                    width: 20,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          width: borderWidth,
                                          color: borderColor,
                                        ),
                                      ),
                                    ),
                                  ),
                                  // PJH, 제목
                                  Expanded(
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(width: borderWidth, color: borderColor),
                                      ),
                                      padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
                                      child: customText(
                                        '${model.feedTitle}',
                                        style: TextStyles.eFeedTitleStyle,
                                      ),
                                    ),
                                  ),
                                  // PJH, 공백
                                  SizedBox(
                                    width: 15,
                                    child: Container(
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                          width: borderWidth,
                                          color: borderColor,
                                        ),
                                      ),
                                    ),
                                  ),
                                  // PJH, 사진
                                  model.lstImagePath!.isNotEmpty
                                      ? Container(
                                          width: 55,
                                          height: 55,
                                          child: CacheImage(
                                            path: model.lstImagePath![0],
                                            fit: BoxFit.cover,
                                            borderRadius: 5.0,
                                          ),
                                        )
                                      : Container(),
                                  // PJH, 공백
                                  model.lstImagePath!.isNotEmpty
                                      ? SizedBox(
                                          width: 15,
                                          child: Container(
                                            decoration: BoxDecoration(
                                              border: Border.all(
                                                width: borderWidth,
                                                color: borderColor,
                                              ),
                                            ),
                                          ),
                                        )
                                      : Container(),
                                ],
                              ),
                            ),
                            // PJH, 공백
                            SizedBox(
                              height: 7,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: borderWidth,
                                    color: borderColor,
                                  ),
                                ),
                              ),
                            ),
                            // PJH, 댓글 & 점포
                            Container(
                              decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                              child: Row(
                                children: [
                                  // PJH, 댓글
                                  Row(
                                    children: [
                                      // PJH, 공백
                                      SizedBox(
                                        width: 20,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              width: borderWidth,
                                              color: borderColor,
                                            ),
                                          ),
                                        ),
                                      ),
                                      // PJH, 댓글
                                      Container(
                                        height: commentSize,
                                        width: commentSize,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                              width: borderWidth,
                                              color: borderColor,
                                            ),
                                            image: DecorationImage(image: Image.asset('assets/images/outline_question_answer_black.png').image)),
                                      ),
                                      Container(
                                        width: commentCountSize,
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            width: borderWidth,
                                            color: borderColor,
                                          ),
                                        ),
                                        child: customText(
                                          model.commentCount.toString(),
                                          style: TextStyles.eFeedCountStyle,
                                        ),
                                      ),
                                      // PJH, 공백
                                      SizedBox(
                                        width: commentSpaceWidth,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              width: borderWidth,
                                              color: borderColor,
                                            ),
                                          ),
                                        ),
                                      ),
                                      // PJH, 싫어요
                                      Container(
                                        height: commentSize,
                                        width: commentSize,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                              width: borderWidth,
                                              color: borderColor,
                                            ),
                                            image: DecorationImage(image: Image.asset('assets/images/outline_thumb_down_alt_black.png').image)),
                                      ),
                                      Container(
                                        width: commentCountSize,
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            width: borderWidth,
                                            color: borderColor,
                                          ),
                                        ),
                                        child: customText(
                                          model.hateCount.toString(),
                                          style: TextStyles.eFeedCountStyle,
                                        ),
                                      ),
                                      // PJH, 공백
                                      SizedBox(
                                        width: commentSpaceWidth,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              width: borderWidth,
                                              color: borderColor,
                                            ),
                                          ),
                                        ),
                                      ),
                                      // PJH, 좋아요
                                      Container(
                                        height: commentSize,
                                        width: commentSize,
                                        decoration: BoxDecoration(
                                            border: Border.all(
                                              width: borderWidth,
                                              color: borderColor,
                                            ),
                                            image: DecorationImage(image: Image.asset('assets/images/outline_thumb_up_alt_black.png').image)),
                                      ),
                                      Container(
                                        width: commentCountSize,
                                        alignment: Alignment.center,
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            width: borderWidth,
                                            color: borderColor,
                                          ),
                                        ),
                                        child: customText(
                                          model.likeCount.toString(),
                                          style: TextStyles.eFeedCountStyle,
                                        ),
                                      ),
                                    ],
                                  ),
                                  // PJH, 점포
                                  Expanded(
                                    child: Container(
                                      padding: EdgeInsets.fromLTRB(0, 0, 10, 0),
                                      alignment: Alignment.centerRight,
                                      child: customText('${model.feedStore}', style: TextStyles.eFeedStoreStyle),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            // PJH, 공백
                            SizedBox(
                              height: 7,
                              child: Container(
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    width: borderWidth,
                                    color: borderColor,
                                  ),
                                ),
                              ),
                            ),
                            // PJH, 라인
                            Container(
                              height: 0.6,
                              color: Colors.grey,
                            )
                          ],
                        ),
                      )
                    :
                    // PJH, Reply 형식
                    type == TweetType.Reply
                        ? Container(decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)), child: Container())
                        :
                        // PJH, Detail 형식
                        type == TweetType.Detail
                            ? Container(
                                decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                child: Container(
                                  padding: EdgeInsets.all(10.0),
                                  child: Column(
                                    children: [
                                      // YEJ, 작성자 추가
                                      Container(
                                        decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                        child: Row(
                                          mainAxisSize: MainAxisSize.max,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                              child: Row(
                                                children: [
                                                  // YEJ, 프로필
                                                  SizedBox(
                                                    width: 42,
                                                    height: 42,
                                                    child: Container(
                                                      child: CircularImage(
                                                        path: model.user!.profilePic,
                                                        empNo: model.user!.empNo,
                                                        isBorder: true,
                                                        borderWidth: 0.5,
                                                        borderColor: Colors.grey,
                                                      ),
                                                    ),
                                                  ),
                                                  // YEJ, 공백
                                                  SizedBox(
                                                    width: 10,
                                                    child: Container(
                                                      decoration: BoxDecoration(
                                                        border: Border.all(
                                                          width: borderWidth,
                                                          color: borderColor,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                  Column(
                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                    children: [
                                                      // YEJ, 작성자명
                                                      SizedBox(
                                                        child: Container(
                                                          child: customText('${model.user!.displayName}', style: TextStyles.eFeedNameStyle),
                                                        ),
                                                      ),
                                                      // YEJ, 공백
                                                      SizedBox(
                                                        height: 5,
                                                        child: Container(
                                                          decoration: BoxDecoration(
                                                            border: Border.all(
                                                              width: borderWidth,
                                                              color: borderColor,
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                      // YEJ, 조직
                                                      SizedBox(
                                                        child: Container(
                                                          child: customText(
                                                            '${model.user!.orgNm}',
                                                            style: TextStyles.eFeedOrgStyle,
                                                            overflow: TextOverflow.ellipsis,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                            // YEJ, 날짜 (오른쪽)
                                            Expanded(
                                              child: Container(
                                                alignment: Alignment.centerRight,
                                                decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                                child: Row(
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  children: [
                                                    // YEJ, 날짜
                                                    customText('${model.createdAt.substring(0, 19)}', style: TextStyles.eDetailFeedDateStyle),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      // YEJ, 공백
                                      SizedBox(height: 10.0),
                                      // PJH, 점포
                                      Container(
                                        decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                        alignment: Alignment.centerLeft,
                                        child: Column(
                                          children: [
                                            // PJH, 점포
                                            Column(
                                              children: [
                                                Container(
                                                  decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                                  alignment: Alignment.centerLeft,
                                                  height: 25,
                                                  child: customText(
                                                    '점포',
                                                    style: TextStyles.eFeedDetailTitle1Style,
                                                  ),
                                                ),
                                                Container(
                                                  decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                                  alignment: Alignment.centerLeft,
                                                  child: customText(
                                                    '여수둔덕점',
                                                    style: TextStyles.eFeedDetailTitle2Style,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            // PJH, 제목
                                            // PJH, 내용 및 요청사항
                                          ],
                                        ),
                                      ),
                                      // PJH, 공백
                                      SizedBox(
                                        height: 12,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              width: borderWidth,
                                              color: borderColor,
                                            ),
                                          ),
                                        ),
                                      ),
                                      // PJH, 제목
                                      Container(
                                        decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                        child: Column(
                                          children: [
                                            // PJH, 제목
                                            Column(
                                              children: [
                                                Container(
                                                  decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                                  alignment: Alignment.centerLeft,
                                                  height: 25,
                                                  child: customText(
                                                    '제목',
                                                    style: TextStyles.eFeedDetailTitle1Style,
                                                  ),
                                                ),
                                                Container(
                                                  decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                                  alignment: Alignment.centerLeft,
                                                  child: customText(
                                                    '${model.feedTitle ?? ""}',
                                                    style: TextStyles.eFeedDetailTitle2Style,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            // PJH, 내용 및 요청사항
                                          ],
                                        ),
                                      ),
                                      // PJH, 공백
                                      SizedBox(
                                        height: 12,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              width: borderWidth,
                                              color: borderColor,
                                            ),
                                          ),
                                        ),
                                      ),
                                      // PJH, 내용 및 요청사항
                                      Container(
                                        decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                        child: Column(
                                          children: [
                                            // PJH, 제목
                                            Column(
                                              children: [
                                                Container(
                                                  decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                                  alignment: Alignment.centerLeft,
                                                  height: 25,
                                                  child: customText(
                                                    '내용 및 요청사항',
                                                    style: TextStyles.eFeedDetailTitle1Style,
                                                  ),
                                                ),
                                                Container(
                                                  decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                                  alignment: Alignment.centerLeft,
                                                  child: customText(
                                                    '${model.feedContent ?? ""}',
                                                    style: TextStyles.eFeedDetailTitle2Style,
                                                  ),
                                                ),
                                              ],
                                            ),
                                            // PJH, 내용 및 요청사항
                                          ],
                                        ),
                                      ),
                                      // PJH, 공백
                                      SizedBox(
                                        height: 12,
                                        child: Container(
                                          decoration: BoxDecoration(
                                            border: Border.all(
                                              width: borderWidth,
                                              color: borderColor,
                                            ),
                                          ),
                                        ),
                                      ),
                                      // PJH, 사진
                                      Container(
                                        decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                        child: Column(
                                          children: [
                                            Column(
                                              children: [
                                                // PJH, 제목
                                                model.lstImagePath!.isNotEmpty
                                                    ? Container(
                                                        decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                                        alignment: Alignment.centerLeft,
                                                        height: 25,
                                                        child: customText(
                                                          '사진',
                                                          style: TextStyles.eFeedDetailTitle1Style,
                                                        ),
                                                      )
                                                    : Container(),
                                                Container(
                                                  decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                                ),
                                                // PJH, 사진
                                                model.lstImagePath!.isNotEmpty
                                                    ? Container(
                                                        decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                                        child: CarouselSlider(
                                                          options: CarouselOptions(
                                                            height: 220.0,
                                                            viewportFraction: 0.6,
                                                            enlargeCenterPage: true,
                                                          ),
                                                          items: model.lstImagePath!.map((filePath) {
                                                            return Builder(
                                                              builder: (BuildContext context) {
                                                                return Container(
                                                                  decoration: BoxDecoration(
                                                                    border: Border.all(width: borderWidth, color: borderColor),
                                                                  ),
                                                                  margin: EdgeInsets.fromLTRB(0, 10, 0, 10),
                                                                  child: GestureDetector(
                                                                    onTap: () {
                                                                      showDialog(
                                                                        context: context,
                                                                        builder: (_) => showPicture(context, filePath),
                                                                      );
                                                                    },
                                                                    child: CacheImage(
                                                                      path: filePath,
                                                                      fit: BoxFit.cover,
                                                                      borderRadius: 6.0,
                                                                    ),
                                                                  ),
                                                                );
                                                              },
                                                            );
                                                          }).toList(),
                                                        ),
                                                      )
                                                    : Container(
                                                        decoration: BoxDecoration(border: Border.all(width: borderWidth, color: borderColor)),
                                                      ),
                                              ],
                                            ),
                                            // PJH, 내용 및 요청사항
                                          ],
                                        ),
                                      ),
                                      // YEJ, 공백
                                      SizedBox(height: 10.0),
                                      // YEJ, 추천 및 비추천 버튼
                                      LikeHateWidget(model),
                                      // YEJ, 공백
                                      SizedBox(height: 10.0),
                                    ],
                                  ),
                                ))
                            : Container()));
  }

  Widget showPicture(context, filePath) {
    // PJH, 테두리
    final borderWidth = 0.1;
    //final borderColor = Colors.red;
    final borderColor = Colors.transparent;

    /*
    return Dialog(
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
              width: borderWidth,
              color: borderColor
          ),
        ),
        child:
        PhotoView(
          imageProvider: customAdvanceNetworkImage(filePath)
        ),
      ),
    );
    */
    return PhotoView(imageProvider: customAdvanceNetworkImage(filePath));
  }
}

class _TweetBody extends StatelessWidget {
  final FeedModel model;
  final Widget? trailing;
  final TweetType type;
  final bool isDisplayOnProfile;
  const _TweetBody({Key? key, required this.model, this.trailing, required this.type, required this.isDisplayOnProfile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // PJH, 폰트 설정
    double descriptionFontSize = type == TweetType.Tweet
        ? 15
        : type == TweetType.Detail || type == TweetType.ParentTweet
            ? 18
            : 14;
    FontWeight descriptionFontWeight = type == TweetType.Tweet || type == TweetType.Tweet ? FontWeight.w400 : FontWeight.w400;
    TextStyle textStyle = TextStyle(color: Colors.black, fontSize: descriptionFontSize, fontWeight: descriptionFontWeight);
    TextStyle urlStyle = TextStyle(color: Colors.blue, fontSize: descriptionFontSize, fontWeight: descriptionFontWeight);
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        // PJH, 타이틀 > 공백
        SizedBox(
          width: 10,
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(
                width: 1,
              ),
            ),
          ),
        ),
        // PJH, 타이틀 > 프로필 사진
        SizedBox(
          width: 40,
          height: 40,
          child: Container(
            child: GestureDetector(
              onTap: () {
                // If tweet is displaying on someone's profile then no need to navigate to same user's profile again.
                if (isDisplayOnProfile) {
                  return;
                }
                Navigator.push(context, ProfilePage.getRoute(profileId: model.userId));
              },
              // PJH, 피드 프로필 사진
              child: CircularImage(path: model.user!.profilePic, empNo: model.user!.empNo),
              /*
            child: CircularImage(path: model.user!.profilePic),
            */
            ),
          ),
        ),
        // PJH, 타이틀 > 공백
        SizedBox(
          width: 20,
          child: Container(),
        ),
        // PJH, 타이틀 > 조직도 & 내용
        SizedBox(
          width: context.width - 80,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Expanded(
                      child: Row(
                        children: <Widget>[
                          ConstrainedBox(
                            constraints: BoxConstraints(minWidth: 0, maxWidth: context.width * .5),
                            child: TitleText(model.user!.displayName!, fontSize: 16, fontWeight: FontWeight.w800, overflow: TextOverflow.ellipsis),
                          ),
                          const SizedBox(width: 3),
                          model.user!.isVerified!
                              ? customIcon(
                                  context,
                                  icon: AppIcon.blueTick,
                                  istwitterIcon: true,
                                  iconColor: AppColor.primary,
                                  size: 13,
                                  paddingIcon: 3,
                                )
                              : const SizedBox(width: 0),
                          SizedBox(
                            width: model.user!.isVerified! ? 5 : 0,
                          ),
                          // PJH, 아이디 제거 및 조직도 추가
                          /*
                          Flexible(
                            child: customText(
                              '${model.user!.userName}',
                              style: TextStyles.userNameStyle,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          */
                          Flexible(
                            child: customText(
                              '${model.user!.orgNm}',
                              style: TextStyles.userNameStyle,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                          const SizedBox(width: 4),
                          customText(
                            '· ${Utility.getChatTime(model.createdAt)}',
                            style: TextStyles.userNameStyle.copyWith(fontSize: 12),
                          ),
                        ],
                      ),
                    ),
                    Container(child: trailing ?? const SizedBox()),
                  ],
                ),
                model.description == null
                    ? const SizedBox()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          UrlText(
                            text: model.description!.removeSpaces,
                            onHashTagPressed: (tag) {
                              cprint(tag);
                            },
                            style: textStyle,
                            urlStyle: urlStyle,
                          ),
                          // TweetTranslation(
                          //   languageCode: model.lanCode,
                          //   tweetKey: model.key!,
                          //   description: model.description!,
                          //   textStyle: textStyle,
                          //   urlStyle: urlStyle,
                          // ),
                        ],
                      ),
                if (model.imagePath == null && model.description != null) CustomLinkMediaInfo(text: model.description!),
              ],
            ),
          ),
        ),
        const SizedBox(width: 10),
      ],
    );
  }
}

class _TweetDetailBody extends StatelessWidget {
  final FeedModel model;
  final Widget? trailing;
  final TweetType type;
  // final bool isDisplayOnProfile;
  const _TweetDetailBody({
    Key? key,
    required this.model,
    this.trailing,
    required this.type,
    /*this.isDisplayOnProfile*/
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double descriptionFontSize = type == TweetType.Tweet
        ? context.getDimention(context, 15)
        : type == TweetType.Detail
            ? context.getDimention(context, 18)
            : type == TweetType.ParentTweet
                ? context.getDimention(context, 14)
                : 10;

    FontWeight descriptionFontWeight = type == TweetType.Tweet || type == TweetType.Tweet ? FontWeight.w300 : FontWeight.w400;
    TextStyle textStyle = TextStyle(color: Colors.black, fontSize: descriptionFontSize, fontWeight: descriptionFontWeight);
    TextStyle urlStyle = TextStyle(color: Colors.blue, fontSize: descriptionFontSize, fontWeight: descriptionFontWeight);
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        model.parentkey != null && model.childRetwetkey == null && type != TweetType.ParentTweet
            ? ParentTweetWidget(
                childRetwetkey: model.parentkey!,
                // isImageAvailable: false,
                trailing: trailing,
                type: type,
              )
            : const SizedBox.shrink(),
        SizedBox(
          width: context.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ListTile(
                contentPadding: const EdgeInsets.symmetric(horizontal: 16),
                leading: GestureDetector(
                  onTap: () {
                    Navigator.push(context, ProfilePage.getRoute(profileId: model.userId));
                  },
                  child: CircularImage(path: model.user!.profilePic, empNo: model.user!.empNo),
                ),
                title: Row(
                  children: <Widget>[
                    ConstrainedBox(
                      constraints: BoxConstraints(minWidth: 0, maxWidth: context.width * .5),
                      child: TitleText(model.user!.displayName!, fontSize: 16, fontWeight: FontWeight.w800, overflow: TextOverflow.ellipsis),
                    ),
                    const SizedBox(width: 3),
                    model.user!.isVerified!
                        ? customIcon(
                            context,
                            icon: AppIcon.blueTick,
                            istwitterIcon: true,
                            iconColor: AppColor.primary,
                            size: 13,
                            paddingIcon: 3,
                          )
                        : const SizedBox(width: 0),
                    SizedBox(
                      width: model.user!.isVerified! ? 5 : 0,
                    ),
                  ],
                ),
                // PJH, SubTitle
                subtitle: customText('${model.user!.userName}', style: TextStyles.userNameStyle),
                trailing: trailing,
              ),
              model.description == null
                  ? const SizedBox()
                  : Padding(
                      padding: type == TweetType.ParentTweet ? const EdgeInsets.only(left: 80, right: 16) : const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          UrlText(
                              text: model.description!.removeSpaces,
                              onHashTagPressed: (tag) {
                                cprint(tag);
                              },
                              style: textStyle,
                              urlStyle: urlStyle),
                          // TweetTranslation(
                          //   languageCode: model.lanCode,
                          //   tweetKey: model.key!,
                          //   description: model.description!,
                          //   textStyle: textStyle,
                          //   urlStyle: urlStyle,
                          // ),
                        ],
                      ),
                    ),
              if (model.imagePath == null && model.description != null)
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: CustomLinkMediaInfo(text: model.description!),
                )
            ],
          ),
        ),
      ],
    );
  }
}
